<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/check_command','Lock\LockController@checkCommand');
Route::post('/make_response','Lock\LockController@makeResponse')->name('make_response');
Route::post('/create_lock','Lock\LockController@createLock')->name('create_lock');
Route::post('/delete_lock','Lock\LockController@deleteLock')->name('delete_lock');
//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'Admin\PagesController@index');
Route::prefix('admin')->name('admin.')->middleware('auth')->group(function () {
    Route::get('/dashboard/{date?}', 'Admin\PagesController@welcome')->name('dashboard');

    Route::prefix('bikes')->name('bikes.')->group(function () {
        Route::get('/', 'Admin\BikeController@getBikes')->name('index');
        Route::get('/add_bike', 'Admin\BikeController@getAddBike')->name('add')->middleware('checkadmin');
        Route::post('/add_bike_p', 'Admin\BikeController@addBike')->name('add_p')->middleware('checkadmin');
        Route::get('/bike/{bike_id}', 'Admin\BikeController@editBike')->name('edit');
        Route::post('/edit/{bike_id}', 'Admin\BikeController@makeBikeChanges')->name('edit_p')->middleware('checkadmin');
        Route::get('/delete/{bike_id}', 'Admin\BikeController@deleteBike')->name('delete')->middleware('checkadmin');
    });
    Route::prefix('models')->middleware('checkadmin')->name('models.')->group(function () {
        Route::get('/', 'Admin\BikeModelController@getModels')->name('index');
        Route::get('/edit/{edit_id}', 'Admin\BikeModelController@editModel')->name('edit');
        Route::post('/model/{edit_id}', 'Admin\BikeModelController@makeModelChanges')->name('edit_p');
        Route::get('/add_model', 'Admin\BikeModelController@getAddModel')->name('add');
        Route::post('/add_model_p', 'Admin\BikeModelController@addModel')->name('add_p');
        Route::get('/delete/{delete_id}', 'Admin\BikeModelController@deleteModel')->name('delete');
    });
    Route::prefix('types')->middleware('checkadmin')->name('types.')->group(function () {
        Route::get('/', 'Admin\BikeTypeController@getTypes')->name('index');
        Route::get('/edit/{edit_id}', 'Admin\BikeTypeController@editType')->name('edit');
        Route::post('/type/{edit_id}', 'Admin\BikeTypeController@makeTypeChanges')->name('edit_p');
        Route::get('/add_type', 'Admin\BikeTypeController@getAddType')->name('add');
        Route::post('/add_type_p', 'Admin\BikeTypeController@addType')->name('add_p');
        Route::get('/delete/{delete_id}', 'Admin\BikeTypeController@deleteType')->name('delete');
    });
    Route::prefix('parkings')->name('parkings.')->group(function () {
        Route::get('/', 'Admin\ParkingController@getParkings')->name('index');
        Route::get('/add_parking', 'Admin\ParkingController@getAddParking')->name('add')->middleware('checkadmin');
        Route::post('/add_parking_p', 'Admin\ParkingController@addParking')->name('add_p')->middleware('checkadmin');
        Route::get('/parking/{parking_id}', 'Admin\ParkingController@editParking')->name('edit');
        Route::post('/edit/{parking_id}', 'Admin\ParkingController@makeParkingChanges')->name('edit_p')->middleware('checkadmin');
        Route::get('/delete/{parking_id}', 'Admin\ParkingController@deleteParking')->name('delete')->middleware('checkadmin');
    });
    Route::prefix('clients')->name('clients.')->group(function () {
        Route::get('/', 'Admin\ClientController@getClients')->name('index');
        Route::get('/add_client', 'Admin\ClientController@getAddClient')->name('add')->middleware('checkadmin');
        Route::post('/add_client_p', 'Admin\ClientController@addClient')->name('add_p')->middleware('checkadmin');
        Route::get('/client/{client_id}', 'Admin\ClientController@editClient')->name('edit')->middleware('checkadmin');
        Route::get('/delete/{client_id}', 'Admin\ClientController@deleteClient')->name('delete')->middleware('checkadmin');
        Route::get('/info/{client_id}', 'Admin\ClientController@getClientInfo')->name('info');
        Route::post('/client/{client_id}', 'Admin\ClientController@makeClientChanges')->name('edit_p')->middleware('checkadmin');
    });
    Route::prefix('trips')->name('trips.')->group(function () {
        Route::get('/', 'Admin\TripController@getTrips')->name('index');
        Route::get('/info/{trip_id}', 'Admin\TripController@getTripInfo')->name('info');
    });
    Route::prefix('payments')->name('payments.')->group(function () {
        Route::get('/', 'Admin\PaymentController@getFinances')->name('index');
    });
    Route::prefix('tickets')->name('tickets.')->group(function () {
        Route::get('/', 'Admin\TicketController@getTickets')->name('index');
        Route::get('/ticket/{ticket_id}', 'Admin\TicketController@getTicket')->name('info');
        Route::get('/changestatus/{ticket_id}/{status_id}', 'Admin\TicketController@changeStatus')->name('changestatus');
        Route::post('/send_message/{ticket_id}', 'Admin\TicketController@sendMessage')->name('send_message');
    });
    Route::prefix('tarifs')->name('tarifs.')->middleware('checkadmin')->group(function () {
        Route::get('/', 'Admin\TarifController@getTarifs')->name('index');
        Route::post('/tarif/{tarif_id}', 'Admin\TarifController@changeTarif')->name('change');
    });
    Route::prefix('promocodes')->middleware('checkadmin')->name('promocodes.')->group(function () {
        Route::get('/add_promocode', 'Admin\PromocodeController@getCreatePromocode')->name('add');
        Route::post('/add_promocode_p', 'Admin\PromocodeController@createPromocode')->name('add_p');
    });
    Route::prefix('promoactions')->middleware('checkadmin')->name('promoactions.')->group(function () {
        Route::get('/', 'Admin\PromoactionController@getPromoactions')->name('index');
        Route::get('/add_promoaction', 'Admin\PromoactionController@getAddPromoaction')->name('add');
        Route::post('/add_promoaction_p', 'Admin\PromoactionController@addPromoaction')->name('add_p');
        Route::get('/promoaction/{promoaction_id}', 'Admin\PromoactionController@editPromoaction')->name('edit');
        Route::post('/edit/{promoaction_id}', 'Admin\PromoactionController@makePromoactionChanges')->name('edit_p');
        Route::get('/delete/{promoaction_id}', 'Admin\PromoactionController@deletePromoaction')->name('delete');
    });
    Route::prefix('users')->middleware('checkadmin')->name('users.')->group(function () {
        Route::get('/', 'Admin\UserController@getUsers')->name('index');
        Route::get('/add_user', 'Admin\UserController@getAddUser')->name('add');
        Route::post('/add_user_p', 'Admin\UserController@addUser')->name('add_p');
        Route::get('/user/{user_id}', 'Admin\UserController@editUser')->name('edit');
        Route::get('/delete/{user_id}', 'Admin\UserController@deleteUser')->name('delete');
        Route::get('/info/{user_id}', 'Admin\UserController@getUserInfo')->name('info');
        Route::post('/user/{user_id}', 'Admin\UserController@makeUserChanges')->name('edit_p');
    });
});