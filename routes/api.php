<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//auth methods
Route::post('send_sms', 'API\AuthController@sendSms');
Route::post('login', 'API\AuthController@login');
Route::post('register', 'API\AuthController@register');

Route::group(['middleware' => 'auth:api'], function(){
    //booking methods
    Route::post('book_bike', 'API\BookController@bookBike');
    Route::post('cancel_booking', 'API\BookController@cancelBooking');
    Route::post('check_booking', 'API\BookController@checkBooking');
    Route::post('calc_booking', 'API\BookController@calcBooking');
    //rent methods
    Route::post('rent_bike', 'API\RentController@rentBike');
    Route::post('done_rent', 'API\RentController@doneRent');
    Route::post('check_trip', 'API\RentController@checkTrip');
    //waiting methods
    Route::post('start_waiting', 'API\WaitController@startWaiting');
    Route::post('stop_waiting', 'API\WaitController@stopWaiting');
    //bikes methods
    Route::post('get_bikes', 'API\BikeController@getBikes');
    //user methods
    Route::post('update_profile', 'API\UserController@updateProfile');
    Route::post('get_current_user', 'API\UserController@getCurrentUser');
    //parkings methods
    Route::post('get_parkings', 'API\ParkingController@getParkings');
    //tickets methods
    Route::post('get_tickets', 'API\TicketController@getTickets');
    Route::post('get_messages', 'API\TicketController@getMessages');
    Route::post('add_ticket', 'API\TicketController@addTicket');
    Route::post('add_message', 'API\TicketController@addMessage');
    //trip history methods
    Route::post('get_trips', 'API\HistoryController@getTrips');
    Route::post('get_trip_details', 'API\HistoryController@getTripDetails');
    //tariffs methods
    Route::post('get_tarifs', 'API\TarifController@getTarifs');
    //promocodes methods
    Route::post('set_promocode', 'API\PromocodeController@setPromocode');
    Route::post('get_promocodes', 'API\PromocodeController@getPromocodes');
    Route::post('get_promoactions', 'API\PromocodeController@getPromoactions');
    //verification methods
    Route::post('verify', 'API\VerificationController@verify');
    Route::post('check_verification', 'API\VerificationController@checkVerification');
    //reports methods
    Route::post('report_bike', 'API\ReportController@reportBike');
    Route::post('report_parking', 'API\ReportController@reportParking');
    //pledges methods
    Route::post('make_pledge', 'API\PledgeController@makePledge');
    Route::post('return_pledge', 'API\PledgeController@returnPledge');

});
