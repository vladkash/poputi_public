const net = require('net');
const lora_packet = require('lora-packet');
var request = require('request');

//server part
const clients = [];


net.createServer(function(socket) {
    socket.name = socket.remoteAddress + ":" + socket.remotePort;
    var headers = {
        'User-Agent':'Super Agent/0.0.1',
        'Content-Type':'application/form-data'
    };
    var options = {
        url: 'http://89.108.64.26/create_lock',
        method: 'POST',
        headers:headers,
        form: {'resource_id': socket.name}
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            // Print out the response body
            if (body == 1) {
                console.log('Lock added');
            }
        }
    });
    clients.push(socket);


    socket.on('data', function(data) {
        console.log("Buffer sent by terminal : ");
        const packet = lora_packet.fromWire(data, 'hex');
        const str = packet._packet.PHYPayload.toString('hex');
        console.log(str);
        var headers = {
            'User-Agent':'Super Agent/0.0.1',
            'Content-Type':'application/form-data'
        };
        var options = {
            url: 'http://89.108.64.26/make_response',
            method: 'POST',
            headers:headers,
            form: {'message': str, 'resource_id': socket.name}
        };
        request(options, function (error, response, body) {
            if (body) {
            socket.write(new Buffer(body.toString("binary"), "hex"));
            console.log("Server answer : ");
                if (body.length > 1000) {
                    console.log('response too long');
                }
                else {
                    console.log(body);
                }
            }
        });
    });
    socket.on('end', function() {
        clients.splice(clients.indexOf(socket), 1);
        console.log('Client disconnected: ' + socket.name);
        var headers = {
            'User-Agent':'Super Agent/0.0.1',
            'Content-Type':'application/form-data'
        };
        var options = {
            url: 'http://89.108.64.26/delete_lock',
            method: 'POST',
            headers:headers,
            form: {'resource_id': socket.name}
        };
        request(options);
    });
    socket.on('error', function (error) {
        clients.splice(clients.indexOf(socket), 1);
        console.log('Client error: ' + socket.name);
        var headers = {
            'User-Agent':'Super Agent/0.0.1',
            'Content-Type':'application/form-data'
        };
        var options = {
            url: 'http://89.108.64.26/delete_lock',
            method: 'POST',
            headers:headers,
            form: {'resource_id': socket.name}
        };
        request(options);
    })
}).listen(2215);

console.log("server is running on the port 2215");

var timerId = setInterval(function() {
    var options = {
        url: 'http://89.108.64.26/check_command',
        method: 'GET'
    };
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            if (body != 0) {
                var data = body.split(' ');
                clients.forEach(function(client) {
                    if (client.name == data[1]) {
                        console.log('Message sent to lock:');
                        console.log(data[0]);
                        client.write(new Buffer(data[0].toString("binary"), "hex"));
                    }
                });
            }
        }
    })
}, 2000);

