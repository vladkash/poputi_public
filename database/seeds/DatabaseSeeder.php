<?php

use App\Bike;
use App\Client;
use App\Parking;
use App\SmsCode;
use App\Tarif;
use Illuminate\Database\Seeder;
use App\User;
use App\TarifType;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        /*User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123890adm'),
        ]);
        Parking::create([
            'name'=>'Парковка 1',
            'count_bike'=>5,
            'is_work'=>1,
            'latitude'=>'47.2295401',
            'longitude'=>'39.6762807'
        ]);
        Bike::create([
            'parking_id'=>1,
            'type_id'=>1,
            'model_id'=>1,
            'serial_num'=>'13211',
            'status_id'=>1,
            'latitude'=>'47.2295101',
            'longitude'=>'39.6762707',
            'sim_num'=>'9231234567'
        ]);
        Bike::create([
            'parking_id'=>1,
            'type_id'=>1,
            'model_id'=>1,
            'serial_num'=>'13212',
            'status_id'=>1,
            'latitude'=>'47.2295101',
            'longitude'=>'39.6762907',
            'sim_num'=>'9231234569'
        ]);
        Tarif::create([
            'tarif_type_id'=>2,
            'beg_minute'=>1,
            'end_minute'=>120,
            'price'=>1.75
        ]);
        Tarif::create([
            'tarif_type_id'=>2,
            'beg_minute'=>121,
            'end_minute'=>240,
            'price'=>1.5
        ]);
        Tarif::create([
            'tarif_type_id'=>2,
            'beg_minute'=>240,
            'end_minute'=>1440,
            'price'=>1.25
        ]);
        Tarif::create([
            'tarif_type_id'=>1,
            'beg_minute'=>1,
            'end_minute'=>1,
            'price'=>1
        ]);
        Tarif::create([
            'tarif_type_id'=>3,
            'beg_minute'=>1,
            'end_minute'=>1,
            'price'=>690
        ]);
        Tarif::create([
            'tarif_type_id'=>4,
            'beg_minute'=>1,
            'end_minute'=>1,
            'price'=>2999
        ]);
        Tarif::create([
            'tarif_type_id'=>5,
            'beg_minute'=>1,
            'end_minute'=>1,
            'price'=>1099
        ]);
        Client::create([
            'phone'=>'+79001975497'
        ]);
        SmsCode::create([
            'phone'=>'+79001975497',
            'code'=>2124
        ]);
        TarifType::create([
            'name'=>'Ожидание'
        ]);
        TarifType::create([
            'name'=>'Поминутно'
        ]);
        TarifType::create([
            'name'=>'Сутки'
        ]);
        TarifType::create([
            'name'=>'Неделя'
        ]);
        TarifType::create([
            'name'=>'Уикенд'
        ]);*/
        /*$client = Client::first();
        $client->payments()->create([
            'type_id'=>1,
            'status_id'=>1,
            'amount'=>500,
            'method'=>'QIWI'
        ]);
        $tarif_type = TarifType::create([
            'name'=>'Залог'
        ]);
        $tarif_type->tarifs()->create([
            'beg_minute'=>1,
            'end_minute'=>1,
            'price'=>500
        ]);*/
        /*DB::table('bike_parts')->insert([
            'name'=>'Руль'
        ]);
        DB::table('bike_parts')->insert([
            'name'=>'Номер'
        ]);
        DB::table('bike_parts')->insert([
            'name'=>'Шина'
        ]);
        DB::table('bike_parts')->insert([
            'name'=>'Седло'
        ]);
        DB::table('bike_parts')->insert([
            'name'=>'Замок'
        ]);
        DB::table('bike_parts')->insert([
            'name'=>'Цепь'
        ]);
        DB::table('bike_parts')->insert([
            'name'=>'Педаль'
        ]);*/

        /*\App\Promoaction::create([
            'name' => 'Тестовая промоакция',
            'amount' => 100,
            'code'=>'TESTACTION777'
        ]);*/

        \App\Role::create([
            'name' => 'admin'
        ]);
        \App\Role::create([
            'name' => 'manager'
        ]);

        App\BikeModel::create([
            'name'=>'Alpine 3000'
        ]);
        App\BikeModel::create([
            'name'=>'City 3000'
        ]);
        App\BikeType::create([
            'name'=>'Прогулочный'
        ]);
        App\BikeType::create([
            'name'=>'Шоссейный'
        ]);
    }
}
