<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parking_id');
            $table->integer('type_id');
            $table->integer('model_id');
            $table->string('serial_num')->unique();
            $table->integer('status_id');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('sim_num')->unique();
            $table->integer('lock_id')->nullable();
            $table->string('bt_mac')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikes');
    }
}
