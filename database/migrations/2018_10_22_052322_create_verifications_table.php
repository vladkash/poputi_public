<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->string('verification_id');
            $table->string('passport_number')->nullable();
            $table->string('patronymic')->nullable();
            $table->string('birthplace')->nullable();
            $table->string('deliverydate')->nullable();
            $table->string('deliveryplace')->nullable();
            $table->string('departmentcode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifications');
    }
}
