<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('phone')->unique();
            $table->string('username')->unique()->nullable();
            $table->string('birthdate')->nullable();
            $table->integer('vote_type')->nullable();
            $table->double('balance')->default(0);
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('photo')->nullable();
            $table->integer('is_vote')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->double('growth')->nullable();
            $table->double('weight')->nullable();
            $table->integer('sex_id')->nullable();
            $table->integer('verificated')->default(0);
            $table->integer('is_locked')->default(0);
            $table->text('additional')->nullable();
            $table->timestamp('lastvisit')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
