
/*!
 * liScroll 1.0
 * Examples and documentation at: 
 * http://www.gcmingati.net/wordpress/wp-content/lab/jquery/newsticker/jq-liscroll/scrollanimate.html
 * 2007-2010 Gian Carlo Mingati
 * Version: 1.0.2.1 (22-APRIL-2011)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * Requires:
 * jQuery v1.2.x or later
 * 
 */


jQuery.fn.liScroll = function(settings) {
		settings = jQuery.extend({
		travelocity: 0.08
		}, settings);		
		return this.each(function(){
				var $strip = jQuery(this);
				$strip.addClass("newsticker")
				var stripWidth = 1;
				$strip.find("li").each(function(i){
				stripWidth += jQuery(this, i).outerWidth(true); // thanks to Michael Haszprunar and Fabien Volpi
				});
				var $mask = $strip.wrap("<div class='mask'></div>");
				var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");								
				var containerWidth = $strip.parent().parent().width();	//a.k.a. 'mask' width 	
				$strip.width(stripWidth);			
				var totalTravel = stripWidth+containerWidth;
				var defTiming = totalTravel/settings.travelocity;	// thanks to Scott Waye		
			
				function scrollnews(spazio, tempo){
				$strip.animate({left: '-='+ spazio}, tempo, "linear", function(){$strip.css("left", containerWidth); scrollnews(totalTravel, defTiming);});
				}
				scrollnews(totalTravel, defTiming);				
				$strip.hover(function(){
				jQuery(this).stop();
				},
				function(){
				var offset = jQuery(this).offset();
				var residualSpace = offset.left + stripWidth;
				var residualTime = residualSpace/settings.travelocity;
				scrollnews(residualSpace, residualTime);
				});			
		});	
};


$(document).ready(function(){
	
	$('.goto_finance').click(function() {
		window.location.href='/user/finance';
	});
	
	$('.regButton').click(function() {
		window.location.href='/register';
	});
	
	$('.authButton').click(function() {
		window.location.href='/login';
	});

	$(".next_delta_delta_q").on("click" , function(){

		console.dir(1);
		$(this).parent().find(".special_mining").slideToggle(400);
		if ($(this).hasClass('rotated')) {
			$(this).removeClass("rotated");
			$(this).css("transform" ,  "rotate(0deg)");
		} else {
			$(this).addClass("rotated");
			$(this).css("transform" , "rotate(180deg)");
		}
	});





	$(".next_delta").on("click" , function(){
		$(this).parent().next().slideToggle(400);
		if ($(this).hasClass('rotated')) {
			$(this).removeClass("rotated");
			$(this).css("transform" ,  "rotate(0deg)");
		} else {
			$(this).addClass("rotated");
			$(this).css("transform" , "rotate(180deg)");
		}
	});

	$(".next_delta_delta").on("click" , function(){
		$(this).parent().parent().next().slideToggle(400);
		if ($(this).hasClass('rotated')) {
			$(this).removeClass("rotated");
			$(this).css("transform" ,  "rotate(0deg)");
		} else {
			$(this).addClass("rotated");
			$(this).css("transform" , "rotate(180deg)");
		}
	});


	if ($("#myChart").length) {
		var ctx = document.getElementById("myChart").getContext('2d');
		ctx.height="450px";
		var gradient = ctx.createLinearGradient(0, 0, 0, 400);
gradient.addColorStop(0, 'rgba(136,108,230,0.4)');   
gradient.addColorStop(1, 'rgba(136,108,230,0)');

var gradient2 = ctx.createLinearGradient(0, 0, 0, 400);
gradient2.addColorStop(0, 'rgba(0,170,255,0.4)');   
gradient2.addColorStop(1, 'rgba(0,170,255,0)');
var myChart = new Chart(ctx, {
   	type: 'line',
    responsive:true,
    maintainAspectRatio: false,
    data: {
        labels: ["12.04.2018", "13.04.2018", "14.04.2018", "15.04.2018", "16.04.2018", "17.04.2018" , "18.04.2018" , "19.04.2018" , "20.04.2018" , "21.04.2018" , "22.04.2018"],
        datasets: [
        {	
            data: [47, 52, 54, 57, 59, 63 , 65 , 68, 71 , 65, 79 ],
            
              pointBackgroundColor: "#fff",
            backgroundColor:gradient,
            borderColor: [
               
                'rgba(136, 108, 230, 1)'

            ],
            borderWidth: 2
        },
        {
            data: [23, 25, 32, 30, 33, 28 , 38 , 42, 45 , 54, 65],
            
            pointBackgroundColor: "#fff",
            backgroundColor:gradient2,
            borderColor: [
                'rgba(0,170,255,1)'
                
            ],
            borderWidth: 2
        }

        ]
    },
    options: {
    	layout:{
    		padding: {
                left: 0,
                right: 4,
                top: 0,
                bottom: 0
            }
    	},
  		tooltips: {
                enabled: true,
                mode: 'single',
                displayColors:false,
                callbacks: {
                    label: function(tooltipItems, data) { 
                        return tooltipItems.yLabel+"€";
                    }
                }
            },
    	elements: {
        line: {
            tension: 0
        }
    },
    	legend:{
    		display:false
    	},
        scales: {
            yAxes: [{
			    gridLines: {
			    	 tickMarkLength: 0,
			        drawBorder: false,
			      },
                ticks: {
                    beginAtZero:true,
                   	stepSize : 25,
                   	min:0,
                   	suggestedMax: 100,
                   	padding:10,
                    callback:function (value , index, values){

                    	if (value != 0) {
                    			return value + "€";
                    		}  else{
                    			return value;
                    		}

                    }

                }
            }],
            xAxes:[{
            	display:false,
            	gridLines:{
            		color:"rgba(0,0,0,0)",
            	},
            }]
        }
    }
});

	}






		$(".logo_block_cashin>div").on("click" , function(){
			$(".logo_block_cashin>div").removeClass("active_cash_in");
			$(this).addClass("active_cash_in");	
		});

		$(".logo_block_cashout>div").on("click" , function(){
			$(".logo_block_cashout>div").removeClass("active_cash_out");
			$(this).addClass("active_cash_out");	
		});
$(".mainText").slick({
		dots:true,
		autoplay:true,
		autoplaySpeed: 3000,
		speed:1500
	});


	$(window).on("scroll" , function(){
		if ($(this).width() > 991) {
			if ($(this).scrollTop() > 1) {

			$(".secondseoncdfirstrow").addClass("actvvv");
			$(".secondfirstrow").addClass("accccc");
			$("header").addClass("acccP");
			$(".secondseoncdfirstrow>.smallBars").css("display"  , "none");
			$(".actvvv .leftPartOut").css({"display" : "flex" , "align-items" : "center" , });
			$(".leftPartOut .smallBars").css("margin-left" , "20px");
			$(".smallopenmenu").css("top" ,  "0px");
			$(".secondseoncdfirstrow.actvvv").css({"border-bottom" :  "0px" ,  "position" : "relative" , "height"  : "40px"});
			$(".fullWrapMe").css("height" , "50px");
			$(".actvvv .leftPart a img").css("width" , "25px");
			$(".smallBars img").css("width"  , "25px");
			$(".smallBars+a img").css("width" , "80px");
			$('.secondseoncdfirstrow ul').removeClass('secondul');
		} else {
			$('.secondseoncdfirstrow.actvvv').css("height" , "auto");
			$(".secondseoncdfirstrow").removeClass("actvvv");
			$(".secondfirstrow").removeClass("accccc");
			$("header").removeClass("acccP");
			$(".fullWrapMe").css("height" , "auto");
			$(".smallBars+a img").css("width" , "135px");
			$('.secondseoncdfirstrow ul').addClass('secondul');

		}
		}
	});

	$(".elemthirdslider").on("mouseenter" ,  function(){
		$(this).find("img").css("transform" , "rotate(360deg)");
		$(this).find(".hilarityBlock").fadeIn(300);
		$(this).find(".hilarityBlock>p").fadeIn(500);
	});
	$(".elemthirdslider").on("mouseleave" ,  function(){
		$(this).find("img").css("transform" , "rotate(0deg)");
		$(this).find(".hilarityBlock").fadeOut(300);
		$(this).find(".hilarityBlock>p").fadeOut(500);
	});
	$(".outerSecondMain ").on("mouseenter" , function(){
		$(this).find("img").css("transform" , "rotate(360deg)");
		$(this).find(".hilarityBlock").fadeIn(800);
		$(this).find(".hilarityBlock>p").fadeIn(900);
	});
	$(".outerSecondMain ").on("mouseleave" , function(){
		$(this).find("img").css("transform" , "rotate(0deg)");
		$(this).find(".hilarityBlock").fadeOut(300);
		$(this).find(".hilarityBlock>p").fadeOut(400);
	});

$("body").on("mouseenter", ".innerSecondMain .slick-next , .innerThirdSlider  .slick-next", function(){
				$(this).css("background-image" , "url('img/rightActive.png')");
				$(this).css("transform" , "rotate(0deg)");
		});
   		$("body").on("mouseenter", ".innerSecondMain .slick-prev , .innerThirdSlider .slick-prev", function(){
			$(this).css("background-image" , "url('img/rightActive.png')");
			$(this).css("transform" , "rotate(-180deg)");
		});


   			$("body").on("mouseleave", ".innerSecondMain .slick-next , .innerThirdSlider  .slick-next", function(){
				$(this).css("background-image" , "url('img/secLeft.png')");
				$(this).css("transform" , "rotate(-180deg)");
		});
   		$("body").on("mouseleave", ".innerSecondMain .slick-prev , .innerThirdSlider .slick-prev", function(){
			$(this).css("background-image" , "url('img/secLeft.png')");
			$(this).css("transform" , "rotate(0deg)");
		});







$("body").on("mouseenter", ".innerTeamBlock .slick-next", function(){
				$(this).css("background-image" , "url('img/rightActive.png')");
				$(this).css("transform" , "rotate(0deg)");
		});
   		$("body").on("mouseenter", ".innerTeamBlock .slick-prev", function(){
			$(this).css("background-image" , "url('img/rightActive.png')");
			$(this).css("transform" , "rotate(-180deg)");
		});


   			$("body").on("mouseleave", ".innerTeamBlock .slick-next", function(){
				$(this).css("background-image" , "url('img/secLeft.png')");
				$(this).css("transform" , "rotate(-180deg)");
		});
   		$("body").on("mouseleave", ".innerTeamBlock .slick-prev", function(){
			$(this).css("background-image" , "url('img/secLeft.png')");
			$(this).css("transform" , "rotate(0deg)");
		});
	$(".burgerMenu img").on("click" , function(){
		$(".menu-overlay").fadeIn(300);
		$(".leftCabin").css("left" , "0");
	});
	$(".burger-over").on("click" , function(){
		$(".leftCabin").css("left" , "-260px");
		$(this).fadeOut(300);
			});


		$(".cabinetUl ul li a").on("mouseenter" ,function(){
			$(this).parent().css("border-left" , "3px solid #fff");
		});

	$(".cabinetUl ul li a").on("mouseleave" ,function(){
			$(this).parent().css("border-left" , "3px solid transparent");
		});

		$('.garmoshkaP').on("click"  , function(){
			if ($(this).hasClass("actgrm")) {
				$(this).removeClass("actgrm");
				$(this).find("img").css("transform" , "rotate(0deg)");
			} else{
				$(this).addClass("actgrm");
				$(this).find("img").css("transform" , "rotate(90deg)");
			}
			$(this).next().slideToggle(300);
		});

		if ($(".innerHotel").length) {
			$(".innerHotel").slick({
				slidesToShow:3,
				responsive: [
   	{
    	breakpoint: 767,
    	settings:{
    	slidesToShow:2
    	}
    },
    {
    	breakpoint:480,
    	settings:{
    		slidesToShow:1
    	}
    }
  ]
			});
		}


		if ($(window).width() < 700) {
			if ($(".innsellblock").length) {

				$(".innsellblock").slick({
					slidesToShow:1
				});
			}
		}
		if ($(".prelastInner").length) {
				if ($(window).width() < 767) {
					$(".prelastInner").slick({
					slidesToShow:1
				});
				}
		}




		$(window).resize(function(){
			if ($(".prelastInner").length) {
				if ($(window).width() < 767) {
					$(".prelastInner").slick({
					slidesToShow:1
				});
				}
			}
			if ($(".innsellblock").length) {
				if ($(window).width() < 700) {
					$(".innsellblock").slick({
					slidesToShow:1
				});
				}
			}
			if ($(".innerTovarBlockMine").length) {
				if ($(window).width() < 640) {
					$(".innerTovarBlockMine").slick({
					slidesToShow:1
				});
				}
			}

			if ($(".sertifInner").length) {
				if ($(window).width() < 767) {
					$(".sertifInner").slick({
					slidesToShow:1
				});
				}
			}

			if ($(".garantueInner").length) {
				if ($(window).width() < 767) {
					$(".garantueInner").slick({
					slidesToShow:1
				});
				}
			}

		if ($(window).width() < 767) {
			if ($(".garantueInner").length) {
				$(".garantueInner").slick({
					slidesToShow:1 ,

				});
			}
		}
		});


		if ($(window).width() < 640) {
			if ($(".innerTovarBlockMine").length) {

				$(".innerTovarBlockMine").slick({
					slidesToShow:1
				});
			}
		}



		if ($(window).width() < 767) {
			if ($(".sertifInner").length) {
				$(".sertifInner").slick({
					slidesToShow:1
				});
			}
			if ($(".garantueInner").length) {
				$(".garantueInner").slick({
					slidesToShow:1
				});
			}
		}


		setInterval(function(){
				var currentBTC = $(".statusBTC>p").text();
				currentBTC = (+ currentBTC + 0.0000012350).toFixed(10);
				$(".statusBTC>p").text(currentBTC);
		},  500);
		$(".smallBars img").on("click" , function(){
			$(".smallopenmenu").css("left" , "0");
			$(".menu-overlay").fadeIn(500);
		});
		$(".menu-overlay").on("click"  ,  function(){
			$(".smallopenmenu").css("left" , "-500px");
			$(".menu-overlay").fadeOut(500);
		});

		$(".marqueediv").liScroll();




	if ($(".innerThirdSliderQ").length  && $(window).width() >= 1200) {
		$(".innerThirdSliderQ").slick({
			slidesToShow:3,
			arrows:true,
			responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2
      }
    },
    {
    	breakpoint: 991,
    	settings:{
    	slidesToShow:1
    	}
    }
  ]
		});
	}

	if ($(".innerThirdSliderQ").length  && $(window).width() >= 991 && $(window).width() < 1200) {
		$(".innerThirdSliderQ").slick({
			slidesToShow:2,
			arrows:true
		});
	}

if ($(".innerThirdSliderQ").length  &&  $(window).width() < 991 && $(window).width() > 767) {
		$(".innerThirdSliderQ").slick({
			slidesToShow:2,
			arrows:true
		});
	}
if ($(".innerThirdSliderQ").length  &&  $(window).width() <=  767) {
		$(".innerThirdSliderQ").slick({
			slidesToShow:1,
			arrows:true
		});
	}








	if ($(".fancyDelta").length) {
			$(".fancyDelta").fancybox();
	}

	

	

	$("#ex1").slider();
	$(".serverInn").on("click" , function(){
		$(".serverInn .serverCheck").removeClass("activeServer");
		$(this).find(".serverCheck").addClass("activeServer");
	});
	$(".dayInner").on("click" , function(){
		$('.dayInner').removeClass("activeDay");
		$(this).addClass("activeDay");
	});

if ($(".partnerInnerBlock").length && $(window).width() >= 1200) {
	if ($(".partnerInnerBlock").length) {
		$(".partnerInnerBlock").slick({
			slidesToShow : 6,
			responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow : 5
      }
    },
    {
    	breakpoint: 991,
    	settings:{
    	slidesToShow : 4	
    	}
    },
    {
    	breakpoint:767,
    	settings:{
    		slidesToShow : 3
    	}
    },
    {
    	breakpoint:580,
    	settings:{
    		slidesToShow:2
    	}
    },
    {
    	breakpoint:440,
    	settings:{
    		slidesToShow:1
    	}
    }
  ]
		});
	}
}
if ($(".partnerInnerBlock").length && $(window).width()  > 991 && $(window).width() < 1200 ) {
		$(".partnerInnerBlock").slick({
			slidesToShow:5,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".partnerInnerBlock").length &&  $(window).width() <= 991 && $(window).width() > 767) {
		$(".partnerInnerBlock").slick({
			slidesToShow:4,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".partnerInnerBlock").length &&  $(window).width() <= 767 && $(window).width() > 580 ) {
		$(".partnerInnerBlock").slick({
			slidesToShow:3,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".partnerInnerBlock").length &&  $(window).width() <= 580 && $(window).width() > 440 ) {
		$(".partnerInnerBlock").slick({
			slidesToShow:2,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".partnerInnerBlock").length &&  $(window).width() < 440 ) {
		$(".partnerInnerBlock").slick({
			slidesToShow:1,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".innerTeamBlock").length && $(window).width() >= 1200) {
		$('.innerTeamBlock').slick({
			slidesToShow:4,
			responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3
      }
    },
    {
    	breakpoint: 991,
    	settings:{
    	slidesToShow:3	
    	}
    },
    {
    	breakpoint:767,
    	settings:{
    		slidesToShow :2
    	}
    },
    {
    	breakpoint:580,
    	settings:{
    		slidesToShow:1
    	}
    }
  ]
		});
	}


	if ($(".innerHardBlock").length && $(window).width() >= 1200) {
		$(".innerHardBlock").slick({
			slidesToShow:3,
			slidesToScroll:1,
			arrows:true,
			responsive: [
    
    {
    	breakpoint: 991,
    	settings:{
    	slidesToShow:2
    	}
    },
    {
    	breakpoint: 767,
    	settings:{
    	slidesToShow:1
    	}
    },
  ]
		});
	}


if ($(".innerTeamBlock").length && $(window).width()  >= 991 && $(window).width() < 1200 ) {
		$(".innerTeamBlock").slick({
			slidesToShow:3,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".innerTeamBlock").length &&  $(window).width() < 991 && $(window).width() > 767) {
		$(".innerTeamBlock").slick({
			slidesToShow:2,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".innerTeamBlock").length &&  $(window).width() <= 767 && $(window).width() > 580 ) {
		$(".innerTeamBlock").slick({
			slidesToShow:2,
			slidesToScroll:1,
			arrows:true
		});
	}


if ($(".innerTeamBlock").length &&  $(window).width() < 580 ) {
		$(".innerTeamBlock").slick({
			slidesToShow:1,
			slidesToScroll:1,
			arrows:true
		});
	}


if ($(".innerHardBlock").length && $(window).width()  >= 991 && $(window).width() < 1200 ) {
		$(".innerHardBlock").slick({
			slidesToShow:2,
			slidesToScroll:1,
			arrows:true
		});
	}

if ($(".innerHardBlock").length &&  $(window).width() < 991 && $(window).width() > 767) {
		$(".innerHardBlock").slick({
			slidesToShow:2,
			slidesToScroll:1,
			arrows:true
		});
	}
if ($(".innerHardBlock").length &&  $(window).width() <= 767 ) {
		$(".innerHardBlock").slick({
			slidesToShow:1,
			slidesToScroll:1,
			arrows:true
		});
	}






	if ($(".miningInnerBlock").length) {
		$(".miningInnerBlock").slick({
			slidesToShow:1,
			slidesToScroll:1,
			dots:true,
			centerMode:false,
				responsive: [
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll:1
		      }
		    },
		    {
		    	breakpoint: 991,
		    	settings:{
		    		slidesToShow:1,
		    		slidesToScroll:1
		    	}
		    },
		    {
		    	breakpoint:767,
		    	settings:{
		    		slidesToShow:1,
		    		slidesToScroll:1
		    	}
		    }
		  ]
		});
		$('.miningInnerBlock').on('afterChange', function(event, slick, currentSlide, nextSlide){
    		if ($(".miningInnerBlock .slick-current").hasClass("russianSlider")) {
    			$(".UkrainianText").css("display" , "none");
    			$(".russianText").fadeIn(300);
    		} else {
    			$(".russianText").css("display" , "none");
    			$(".UkrainianText").fadeIn(300);
    		}
});
	}
	// if ($(".innerSecondMain").length) {
	// $(".innerSecondMain").slick({
	// 	slidesToShow:4,
	// 	responsive: [
 //    {
 //      breakpoint: 1200,
 //      settings: {
 //        arrows: true,
 //        centerMode: true,
 //        centerPadding: '10px',
 //        slidesToShow: 3
 //      }
 //    },
 //    {
 //    	breakpoint: 991,
 //    	settings:{
 //    		slidesToShow:2
 //    	}
 //    },
 //    {
 //    	breakpoint:767,
 //    	settings:{
 //    		slidesToShow:1
 //    	}
 //    }
 //  ]

	// });
	// }



	if ($(".innerSecondMain").length  && $(window).width() >= 1200) {
		$(".innerSecondMain").slick({
			slidesToShow:3,
			arrows:true,
			responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2
      }
    },
    {
    	breakpoint: 991,
    	settings:{
    	slidesToShow:1
    	}
    }
  ]
		});
	}


	if ($(".innerSecondMain").length  && $(window).width() >= 991 && $(window).width() < 1200) {
		$(".innerSecondMain").slick({
			slidesToShow:2,
			arrows:true
		});
	}

if ($(".innerSecondMain").length  &&  $(window).width() < 991 && $(window).width() > 767) {
		$(".innerSecondMain").slick({
			slidesToShow:1,
			arrows:true
		});
	}
if ($(".innerSecondMain").length  &&  $(window).width() <=  767) {
		$(".innerSecondMain").slick({
			slidesToShow:1,
			arrows:true
		});
	}








	$(".leftTouchv").on("click" , function(){
			$(".partnerInnerBlock .slick-prev").trigger("click");
	});
	$(".rightTouchv").on("click" , function(){
			$(".partnerInnerBlock .slick-next.slick-arrow").trigger("click");
	});



	$(".leftTouchme").on("click" , function(){
			$(".innerTovarBlockMine .slick-prev").trigger("click");
	});
	$(".rightTouchme").on("click" , function(){
			$(".innerTovarBlockMine .slick-next.slick-arrow").trigger("click");
	});



	$(".leftTouchmee").on("click" , function(){
			$(".innsellblock .slick-prev").trigger("click");
	});
	$(".rightTouchmee").on("click" , function(){
			$(".innsellblock .slick-next.slick-arrow").trigger("click");
	});



	$(".leftTouchz").on("click" , function(){
			$(".innerTeamBlock .slick-prev").trigger("click");
	});
	$(".rightTouchz").on("click" , function(){
			$(".innerTeamBlock .slick-next.slick-arrow").trigger("click");
	});

	$(".leftTouch").on("click" , function(){
			$(".innerSecondMain .slick-prev").trigger("click");
	});
	$(".rightTouch").on("click" , function(){
			$(".innerSecondMain .slick-next.slick-arrow").trigger("click");
	});

	$(".leftTouchn").on("click" , function(){
			$(".outerThirdSlider .slick-prev").trigger("click");
	});
	$(".rightTouchn").on("click" , function(){
			$(".outerThirdSlider .slick-next.slick-arrow").trigger("click");
	});

$(".leftTouchpls").on("click" , function(){
			$(".innerHotel .slick-prev").trigger("click");
	});
	$(".rightTouchpls").on("click" , function(){
			$(".innerHotel .slick-next.slick-arrow").trigger("click");
	});

	$(".leftTouchp").on("click" , function(){
			$(".sertifInner  .slick-prev").trigger("click");
	});
	$(".rightTouchp").on("click" , function(){
			$(".sertifInner  .slick-next.slick-arrow").trigger("click");
	});

	$(".leftTouchb").on("click" , function(){
			$(".garantueOuter  .slick-prev").trigger("click");
	});
	$(".rightTouchb").on("click" , function(){
			$(".garantueOuter  .slick-next.slick-arrow").trigger("click");
	});

	$(".innernnerHardBlock .leftTouchnq").on("click" , function(){
			$(".innerHardBlock .slick-prev").trigger("click");
	});
	$(".innernnerHardBlock  .rightTouchnq").on("click" , function(){
			$(".innerHardBlock .slick-next.slick-arrow").trigger("click");
	});

$(".preprepreblock .leftTouchpre").on("click" , function(){
			$(".prelastInner .slick-prev").trigger("click");
	});
	$(".preprepreblock  .rightTouchpre").on("click" , function(){
			$(".prelastInner .slick-next.slick-arrow").trigger("click");
	});


});