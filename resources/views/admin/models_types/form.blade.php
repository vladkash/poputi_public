@extends("admin.layouts.layout")

@section("content")
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{route('admin.bikes.index')}}" title="Велосипеды">Велосипеды</a></li>
            <li>{{ $title }}</li>
        </ul>
        <div class="clearfix"></div>
        <div class="header">
            <h1>{{ $title }}</h1>
        </div>
        <div class="clearfix"></div>
        @if (isset($edit))
            <form action="{{ route($route_name.'_p',['edit_id'=>$edit->id]) }}" method="post">
                @else
                    <form action="{{ route($route_name.'_p') }}" method="post">
                        @endif
                        @csrf
                        <div class="col-md-6" style="padding: 0;">
                            @if (isset($errors))
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-warning">{{ $error }}</div>
                                @endforeach
                            @endif
                            <div class="form-group">
                                <label for="edit_name">Имя</label>
                                <input id="edit_name" type="text" name="edit_name" value="@if(isset($edit)){{ $edit->name }}@else{{old('edit_name')}}@endif" class="form-control">
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" value="{{ $title }}" class="form-control btn btn-primary">
                                @if(isset($edit))
                                <a href="{{route('admin.'.$delete.'.delete',['delete_id'=>$edit->id])}}" class="form-control btn btn-danger">Удалить</a>
                                @endif
                            </div>
                        </div>
                    @if(isset($edit))    
                    </form>
                    @else
            </form>
        @endif
    </div>
@endsection