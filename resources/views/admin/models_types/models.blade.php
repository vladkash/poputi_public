@extends("admin.layouts.layout")

@section("content")
    <div class="container atab_list atab">
        <div class="clearfix"></div>
        <div class="header">
            <h1>Модели велосипедов</h1>
        </div>
        <ul class="buttons">
            <li><a href="{{route('admin.models.add')}}">Добавить модель</a></li>
        </ul>
        <div class="clearfix"></div>
        <br>

        <div class="container" style="margin-top: 20px;">
            <div class="">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Имя</th>
                    </tr>
                    </thead>
                    <tbody id="search-table">
                    @foreach ($models as $model)
                        <tr>
                            <td scope="row">{{$model->id}}</td>
                            <td scope="row"><a href="{{route('admin.models.edit', ['model_id'=>$model->id])}}">{{$model->name}}</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection