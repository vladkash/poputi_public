@extends("admin.layouts.layout")

@section("content")
    <div class="container atab_list atab">
        <div class="clearfix"></div>
        <div class="header">
            <h1>Менеджеры</h1>
        </div>
        <ul class="buttons">
            <li><a href="{{route('admin.users.add')}}">Добавить пользователя</a></li>
        </ul>
        <div class="clearfix"></div>
        <br>

        <div class="container" style="margin-top: 20px;">
            <div class="">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Email</th>
                        <th scope="col">Роль</th>
                    </tr>
                    </thead>
                    <tbody id="search-table">
                    @foreach ($users as $user)
                        <tr>
                            <td scope="row">{{$user->id}}</td>
                            <td><a href="{{route('admin.users.edit', ['user_id'=>$user->id])}}">{{$user->name}}</a></td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role->name}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection