@extends("admin.layouts.layout")

@section("content")
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{route('admin.users.index')}}" title="Промокоды">Пользователи</a></li>
            <li>{{ $title }}</li>
        </ul>
        <div class="clearfix"></div>
        <div class="header">
            <h1>{{ $title }}</h1>
        </div>
        <div class="clearfix"></div>
        @if (isset($user))
            <form action="{{ route($route_name.'_p',['user_id'=>$user->id]) }}" method="post">
                @else
                    <form action="{{ route($route_name.'_p') }}" method="post">
                        @endif
                        @csrf
                        <div class="col-md-6" style="padding: 0;">
                            @if (isset($errors))
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-warning">{{ $error }}</div>
                                @endforeach
                            @endif
                            <div class="form-group">
                                <label for="user_name">Имя</label>
                                <input id="user_name" type="text" name="name" value="@if(isset($user)){{ $user->name }}@else{{old('name')}}@endif" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="user_email">Email</label>
                                <input id="user_email" type="email" name="email" value="@if(isset($user)){{ $user->email }}@else{{old('email')}}@endif" class="form-control">
                            </div>
                            {{--<div class="form-group">
                                <label for="user_role">Роль</label>
                                <select id="user_role" name="role_id" class="form-control">
                                    <option value="">Выберите</option>
                                    @foreach (\App\Role::all() as $role)
                                        <option value="{{ $role->id }}" @if (isset($user)) @if ($user->role_id == $role->id) selected @endif @endif>{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>--}}
                            @if(!isset($user))
                            <div class="form-group">
                                <label for="user_pass">Пароль</label>
                                <input id="user_pass" type="password" name="password" placeholder="Оставьте пустым для случайной генерации" class="form-control">
                            </div>
                            @endif
                            <div class="form-group">
                                <input type="submit" value="{{ $title }}" class="form-control btn btn-primary">
                                @if(isset($user))
                                    <a href="{{route('admin.users.delete',['user_id'=>$user->id])}}" class="form-control btn btn-danger">Удалить</a>
                                @endif
                            </div>
                        </div>
                        @if(isset($user))
                    </form>
                    @else
            </form>
        @endif
    </div>
@endsection