@extends("admin.layouts.layout")
@section("content")
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
            <li><a href="{{route('admin.users.index')}}" title="Пользователи">Пользователи</a></li>
            <li>Карточка менеджера</li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="container">
        <div class="atab atab_info">
            <div class="row user">
                <div class="col-md-8">
                    <table class="user">
                        <tr>
                            <td>ID</td>
                            <td>{{ $user->id }}</td>
                        </tr>
                        <tr>
                            <td>Имя</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <td>Аккаунт создан</td>
                            <td>{{ date('H:i d.m.Y', strtotime($user->created_at)) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

