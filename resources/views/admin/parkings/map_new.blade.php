<script>
    ymaps.ready(function () {
        var myMap = new ymaps.Map('velo_map', {
            center: [47.2295101, 39.6762907],
            zoom: 12
        }, {
            searchControlProvider: 'yandex#search'
        });
        myMap.events.add('click',function (e) {
            var coords = e.get('coords');
            if($('#need_first_point').val() == '1'){
                $('#need_first_point').val('0');
                $('#latitude1').val(coords[0]);
                $('#longitude1').val(coords[1]);
                return "first point added";
            }
            if($('#need_second_point').val() == '1'){
                $('#need_second_point').val('0');
                $('#latitude2').val(coords[0]);
                $('#longitude2').val(coords[1]);
                return "second point added";
            }
            var is_placemark = false;
            myMap.geoObjects.each(function (object) {
                if (object.name_type == 'placemark') {
                    is_placemark = true;
                }
            });
            if (is_placemark) {
                return "objects created"
            }
            var myPlacemark = new ymaps.Placemark([coords[0],coords[1]], {
                hintContent: 'Метка парковки',
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: '{{url('/img/parking.png')}}',
                // Размеры метки.
                iconImageSize: [70, 100],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-35, -75],
                draggable: true
            });
            $('#parking_lat').val(coords[0]);
            $('#parking_lng').val(coords[1]);
            myPlacemark.events.add('dragend', function (e) {
                var drag_coords = myPlacemark.geometry.getCoordinates();
                $('#parking_lat').val(drag_coords[0]);
                $('#parking_lng').val(drag_coords[1]);
            });
            myPlacemark.name_type = 'placemark';
            myMap.geoObjects.add(myPlacemark);
        });
        $('#delete_rectangle').click(function () {
            myMap.geoObjects.each(function (object) {
                if (object.name_type == 'rectangle') {
                    myMap.geoObjects.remove(object);
                }
                $('#latitude1').val('Щелкните по карте');
                $('#longitude1').val('Щелкните по карте');
                $('#latitude2').val('Щелкните по карте');
                $('#longitude2').val('Щелкните по карте');
                $('#need_first_point').val('1');
                $('#need_second_point').val('1');
            });
        });
        $('#make_rectangle').click(function () {
            var is_rectangle = false;
            myMap.geoObjects.each(function (object) {
                if (object.name_type == 'rectangle') {
                    alert('Область уже существует! Для создания новой удалите старую!')
                    is_rectangle = true;
                }
            });
            if (is_rectangle) {
                return "rectangle already exists!";
            }
            var myRectangle = new ymaps.Rectangle([
                [parseFloat($('#latitude1').val()),parseFloat($('#longitude1').val())],
                [parseFloat($('#latitude2').val()),parseFloat($('#longitude2').val())]
            ],{},{draggable:true});
            if (!myRectangle) {
                alert('Ошибка создания области!');
                return false;
            }
            myRectangle.events.add('dragend',function (e) {
                var drag_coords = myRectangle.geometry.getCoordinates();
                $('#latitude1').val(drag_coords[0][0]);
                $('#longitude1').val(drag_coords[0][1]);
                $('#latitude2').val(drag_coords[1][0]);
                $('#longitude2').val(drag_coords[1][1]);
            });
            myRectangle.name_type = 'rectangle';
            myMap.geoObjects.add(myRectangle);
        })
    });
</script>