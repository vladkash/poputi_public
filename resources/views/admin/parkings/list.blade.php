@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Парковки</li>
		</ul>

	</div>
	<div class="container-fluid map atab_map atab">
		<div class="container">
			<div class="clearfix"></div>
			<div class="header">
				<h1>Парковки (карта)</h1>
				<ul>
					<li><a href="#" data-tab="map" class="mode_map rtool active a_tab"></a></li>
					<li><a href="#" data-tab="list" class="mode_table rtool a_tab"></a></li>
				</ul>
			</div>
			<ul class="buttons">
				<li><a href="{{route('admin.parkings.add')}}">Добавить парковку</a></li>
			</ul>
			<div class="clearfix"></div>
			<br>
		</div>
		<div id="velo_map"></div>
	</div>
	<div class="container atab_list atab" style="display: none">
		<div class="clearfix"></div>
		<div class="header">
			<h1>Парковки (список)</h1>
			<ul>
				<li><a href="#" data-tab="map" class="mode_map rtool a_tab"></a></li>
				<li><a href="#" data-tab="list" class="mode_table rtool a_tab active"></a></li>
			</ul>
		</div>
		<ul class="buttons">
			<li><a href="{{route('admin.parkings.add')}}">Добавить парковку</a></li>
		</ul>
		<div class="clearfix"></div>
		<br>

		<div style="margin-top: 20px; ">
			<input id="search" class="form-control input-lg" type="text" placeholder="Начните вводить...">
		</div>
		<div id="search-result">
			@include("admin.parkings.table")
			{{$parkings->links('admin.layouts.pagination')}}
		</div>
	</div>
	@endsection
	@section("js")
	<script>
        $('#search').on('input change', function (e) {
            e.preventDefault();

            $.ajax({
                url: "{{route('admin.parkings.index')}}",
                type: "GET",
                data: {search_park: $(this).val()},
                success: function (res) {
                    $('#search-result').html(res);
                }
            });
        });
        ymaps.ready(function () {
            var myMap = new ymaps.Map('velo_map', {
                    center: [47.2295101, 39.6762907],
                    zoom: 12
                }, {
                    searchControlProvider: 'yandex#search'
                }),
				@foreach($parkings as $parking)
				@if($parking->latitude && $parking->longitude)
                    myPlacemark = new ymaps.Placemark([parseFloat("{{$parking->latitude}}"),parseFloat("{{$parking->longitude}}")], {
                        hintContent: 'Пароковка {{$parking->id}}, количество велосипедов на парковке: {{$parking->bikes()->count()}}',
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#image',
                        // Своё изображение иконки метки.
                        iconImageHref: '{{url('/img/parking.png')}}',
                        // Размеры метки.
                        iconImageSize: [70, 100],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-35, -75]
                    });
					myPlacemark.events.add('click',function () {
                	window.open('{{route('admin.parkings.edit',['parking_id'=>$parking->id])}}','_blank');
            		});
					myMap.geoObjects.add(myPlacemark);
            		@if($parking->latitude1 && $parking->longitude1 && $parking->latitude2 && $parking->longitude2)
					var myRectangle = new ymaps.Rectangle([
                    	[parseFloat("{{$parking->latitude1}}"),parseFloat("{{$parking->longitude1}}")],
                    	[parseFloat("{{$parking->latitude2}}"),parseFloat("{{$parking->longitude2}}")]
					]);
           			myMap.geoObjects.add(myRectangle);
					@endif
       		@endif
			@endforeach
        });
        $('#velo_map').css('height', $(document).height()/1.5);
	</script>
	<script>
		$(document).ready(function() {
			$('.a_tab').click(function() {
				this_tab = $(this).data('tab');
				this_t = $('.atab_'+this_tab);
				if (!this_t.length) {	
					return false;
				}
				type = $(this).attr('data-tab');
				$('.a_tab').removeClass('active');
				$('.a_tab[data-tab='+type+']').addClass('active');
				$('.atab').fadeOut('fast');
				$(this_t).fadeIn('fast');
			});
			
		});
	</script>
@endsection
