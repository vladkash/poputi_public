<table class="table">
    <thead>
    <tr>
        <th scope="col">№</th>
        <th scope="col">Кол-во велосипедов</th>
        <th scope="col">Статус парковки</th>
        <th scope="col">GPS</th>
    </tr>
    </thead>
    <tbody id="search-table">
    @foreach($parkings as $parking)
        <tr class="{{ $parking->class}}">
            <th scope="row"><a href="{{ route('admin.parkings.edit',['parking_id'=>$parking->id])}}">{{ $parking->id }}</a></td>
            <td>{{$parking->bikes?$parking->bikes()->count() : 0}}</td>
            <td>{{ $parking->status }}</td>
            <td>{{ $parking->latitude }}, {{ $parking->longitude }}</td>
        </tr>
    @endforeach
    </tbody>
</table>