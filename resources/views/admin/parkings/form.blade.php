@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>{{ $title }}</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>{{ $title }}</h1>
		</div>
		<div class="clearfix"></div>
		@if (isset($parking))
			<form action="{{ route('admin.parkings.edit_p', ['parking_id'=>$parking->id]) }}" method="post">
		@else
			<form action="{{ route('admin.parkings.add_p') }}" method="post">
		@endif
			<div class="col-md-6" style="padding: 0;">
				@if (isset($errors))
					@foreach($errors->all() as $error)
						<div class="alert alert-warning">{{ $error }}</div>
					@endforeach
				@endif
				@csrf
				<div class="form-group">
					<label for="bike_id">ID парковки</label>
					<input id="bike_id" name="id" value="@if (isset($parking)) {{ $parking->id }} @endif" readonly class="form-control">
				</div>
				<div class="form-group">
					<label for="parking_id">Название парковки</label>
					<input id="parking_id" type="text" name="name" value="@if (isset($parking)) {{ $parking->name }}@else{{old('name')}}@endif" class="form-control">
				</div>
				<div class="form-group">
					<label for="parking_id">Кол-во велосипедов</label>
					<input id="parking_id" type="text" readonly value="@if (isset($parking)) {{ $parking->bikes()->count()}} @else 0 @endif" class="form-control">
				</div>
				<div class="form-group">
					<label for="parking_work">Состояние</label>
					<select id="parking_work" name="is_work" class="form-control">
						<option value="">Выберите</option>
						<option value="1" @if (isset($parking)) @if ($parking->is_work == 1) selected @endif @endif>Работает</option>
						<option value="0" @if (isset($parking)) @if ($parking->is_work == 0) selected @endif @endif>Не работает</option>
					</select> 
				</div>
				<div class="form-group">
					<label for="parking_lat">Широта</label>
					<input id="parking_lat" type="text" name="latitude" placeholder="Кликните по карте" value="@if (isset($parking)) {{ $parking->latitude }} @endif" class="form-control" readonly>
				</div>
				<div class="form-group">
					<label for="parking_lng">Долгота</label>
					<input id="parking_lng" type="text" name="longitude" placeholder="Кликните по карте" value="@if (isset($parking)) {{ $parking->longitude }} @endif" class="form-control" readonly>
				</div>
					<input type="hidden" id="form_latitude1" name="latitude1">
					<input type="hidden" id="form_longitude1" name="longitude1">
					<input type="hidden" id="form_latitude2" name="latitude2">
					<input type="hidden" id="form_longitude2" name="longitude2">
				<div class="form-group">
					<input type="submit" id="main_form_submit" name="done" value="{{ $title }}" class="form-control btn btn-primary">
					@if(isset($parking))
						<a href="{{route('admin.parkings.delete',['parking_id'=>$parking->id])}}" class="form-control btn btn-danger">Удалить парковку</a>
					@endif
				</div>
			</div>
			<div class="col-md-6" style="padding-top: 25px;">
				<div id="velo_map" style="height: 370px;"></div>
				<div class="form-group">
					<input type="button" id="rectangle_form_button" value="Редактировать область парковки" class="form-control btn btn-primary">
				</div>
				<div id="rectangle_form" style="display: none;width: 100%;">
					<div class="col-md-6 form-group">
						<label for="latitude1">Широта первой точки</label>
						<input type="text" class="form-control" id="latitude1" value="{{isset($parking)? ($parking->latitude1?$parking->latitude1:'Щелкните по карте') : 'Щелкните по карте'}}">
					</div>
					<div class="col-md-6 form-group">
						<label for="longitude1">Долгота первой точки</label>
						<input type="text" class="form-control" id="longitude1" value="{{isset($parking)? ($parking->longitude1?$parking->longitude1:'Щелкните по карте') : 'Щелкните по карте'}}">
					</div>
					<input type="hidden" id="need_first_point" value="0">
					<div class="col-md-6 form-group">
						<label for="latitude2">Широта второй точки</label>
						<input type="text" class="form-control" id="latitude2" value="{{isset($parking)? ($parking->latitude2?$parking->latitude2:'Щелкните по карте') : 'Щелкните по карте'}}">
					</div>
					<div class="col-md-6 form-group">
						<label for="longitude2">Долгота второй точки</label>
						<input type="text" class="form-control" id="longitude2" value="{{isset($parking)? ($parking->longitude2?$parking->longitude2:'Щелкните по карте') : 'Щелкните по карте'}}">
					</div>
					<input type="hidden" id="need_second_point" value="0">
					<div class="col-md-6">
						<input type="button" id="delete_rectangle" value="Удалить существующую область" class="form-control btn btn-danger">
					</div>
					<div class="col-md-6">
						<input type="button" id="make_rectangle" value="Построить область по точкам" class="form-control btn btn-primary">
					</div>
				</div>
			</div>
		@if (isset($parking))
			</form>
		@else
			</form>
		@endif
	</div>
	@if(isset($parking))
	<div class="container" style="margin-top: 20px;">
		<div class="">
			@include('admin.bikes.table')
			{{$bikes->links('admin.layouts.pagination')}}
		</div>
	</div>
	@endif
	@endsection
	@section("js")
	<script>
        $('#main_form_submit').click(function () {
            if (parseFloat($('#latitude1').val())) {
                $('#form_latitude1').val($('#latitude1').val());
            }
            if (parseFloat($('#latitude1').val())) {
                $('#form_latitude2').val($('#latitude2').val());
            }
            if (parseFloat($('#latitude1').val())) {
                $('#form_longitude1').val($('#longitude1').val());
            }
            if (parseFloat($('#latitude1').val())) {
                $('#form_longitude2').val($('#longitude2').val());
            }
        });
		
        $('#rectangle_form_button').click(function () {
            $('#rectangle_form_button').hide();
			@if(!isset($parking) || !$parking->latitude1 || $parking->latitude1 == "Щелкните по карте")
            $('#need_first_point').val('1');
            $('#need_second_point').val('1');
			@endif
			$('#rectangle_form').show();
        });
    </script>

    @if(isset($parking))
        @include("admin.parkings.map_edit")
    @else
        @include("admin.parkings.map_new")
    @endif
@endsection