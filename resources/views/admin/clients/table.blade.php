<table class="table">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Имя</th>
        <th scope="col">Фамилия</th>
        <th scope="col">Email</th>
        <th scope="col">Телефон</th>
        <th scope="col">Дата рожд</th>
        <th scope="col">Статус</th>
        <th scope="col">Баланс</th>
        <th scope="col">Кол. поездок</th>
    </tr>
    </thead>
    <tbody id="search-table">
    @foreach ($clients as $client)
        <tr>
            <th scope="row"><a href="{{route('admin.clients.info',['client_id'=>$client->id])}}">{{ $client->id }}</a></th>
            <td>{{ $client->name }}</td>
            <td>{{ $client->surname }}</td>
            <td>{{ $client->email }}</td>
            <td>{{ $client->phone }}</td>
            <td>{{ date('d.m.Y',strtotime($client->birthdate ))}}</td>
            <td>
                @if ($client->id_locked)
                    Заблокирован
                @else
                    Активен
                @endif
            </td>
            <td>{{$client->balance?$client->balance:0}} руб.</td>
            <td>{{$client->trips()? $client->trips()->where('done', 1)->count():0}}</td>
        </tr>
    @endforeach
    </tbody>
</table>