@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Карточка пользователя</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>Карточка пользователя</h1>
			<ul class="buttons">
				<li><a href="#" class="a_tab active" data-tab="info">Информация о пользователе</a></li>
				<li><a href="#" data-tab="trips" class="a_tab">Все поездки</a></li>
				<li><a href="#" data-tab="finance" class="a_tab">Финансовые операции</a></li>
			</ul>
		</div>
	</div>
	<div class="container">
		<div class="atab atab_info">
			<div class="row user">
				<div class="col-md-4">
					<img src="{{$client->photo? url('storage/avatars/'.$client->photo):'http://weedoo.ru/public/img/ava_user.png'}}" style="width:370px; height: auto" >
					<ul class="buttons">
						<li><a href="{{route('admin.clients.edit',['client_id'=>$client->id])}}" class="active">Редактировать</a></li>
						<li style="float: right;"><a href="{{route('admin.clients.delete',['client_id'=>$client->id])}}" class="active" >Удалить</a></li>
					</ul>
				</div>
				<div class="col-md-8">
					<table class="user">
					<tr>
						<td>ID</td>
						<td>{{ $client->id }}</td>
					</tr>
					<tr>
						<td>Имя</td>
						<td>{{ $client->name }}</td>
					</tr>
					<tr>
						<td>Фамилия</td>
						<td>{{ $client->surname }}</td>
					</tr>
					<tr>
						<td>Email</td>
						<td>{{ $client->email }}</td>
					</tr>
					<tr>
						<td>Телефон</td>
						<td>{{ $client->phone }}</td>
					</tr>
					<tr>
						<td>Дата/мес/год рожд</td>
						<td>{{ $client->birthdate }}</td>
					</tr>
					<tr>
						<td>Рост</td>
						<td>{{ $client->growth }}</td>
					</tr>
					<tr>
						<td>Вес</td>
						<td>{{ $client->weight }}</td>
					</tr>
					<tr>
						<td>Дата время регистрации</td>
						<td>{{date('H:i d.m.Y',strtotime($client->created_at))}}</td>
					</tr>
					<tr>
						<td>Дата посл. входа</td>
						<td>{{date('H:i d.m.Y', strtotime($client->lastvisit))}}</td>
					</tr>
					<tr>
						<td>Статус</td>
						<td>
							@if ($client->is_locked)
								Заблокирован
							@else
								Активен
							@endif
						</td>
					</tr>
					<tr>
						<td>Баланс</td>
						<td>{{$client->balance}}</td>
					</tr>
					<tr>
						<td>Сумма оплат</td>
						<td>{{$client->payments_amount}}</td>
					</tr>
					<tr>
						<td>Пол</td>
						<td>
							@if ($client->sex_id)
								Мужской
							@else
								Женский
							@endif
						</td>
					</tr>
					<tr>
						<td>Верификация</td>
						<td>
							@if ($client->verificated)
								Верифицирован
							@else
								Не верифицирован
							@endif
						</td>
					</tr>
					<tr>
						<td>Социальный статус</td>
						<td>&mdash;</td>
					</tr>
					<tr>
						<td>Наличие залога</td>
						<td>&mdash;
						@if($client->pledge)
							Да
						@else
							Нет
						@endif
						</td>
					</tr>
					<tr>
						<td>Количество активированных промокодов</td>
						<td>&mdash;{{$client->promocodes? $client->promocodes()->count():0 }}</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="atab atab_trips" style="display: none;">
			@include('admin.trips.table')
            {{$trips->links('admin.layouts.pagination')}}
		</div>
		<div class="atab atab_finance" style="display: none;">
			@include('admin.payments.table')
            {{$payments->links('admin.layouts.pagination')}}
		</div>
	</div>
@endsection
@section("js")
	<script>
		$(document).ready(function() {
			$('.a_tab').click(function() {
				this_tab = $(this).data('tab');
				this_t = $('.atab_'+this_tab);
				if (!this_t.length) {					
					return false;
				}
				$('.a_tab').removeClass('active');
				$(this).addClass('active');
				$('.atab').fadeOut('fast');
				$(this_t).fadeIn('fast');
			});
			
		});
	</script>
@endsection
