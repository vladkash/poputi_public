@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Пользователи сервиса</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>Пользователи сервиса</h1>
		</div>
		<ul class="buttons">
			<li><a href="{{route('admin.clients.add')}}">Добавить пользователя</a></li>
			<li><a href="{{route('admin.users.index')}}">Менеджеры сервиса</a></li>
		</ul>
	</div>
	
	<div class="container" style="margin-top: 20px;">
		<input id="search" class="form-control input-lg" type="text" placeholder="Начните вводить...">
	</div>
	<div class="container">
		<div id="search-result">
			@include("admin.clients.table")
		</div>
		{{$clients->links('admin.layouts.pagination')}}
	</div>
@endsection
@section("js")
	<script type="text/javascript">
        $('#search').on('input change', function (e) {
            e.preventDefault();

            $.ajax({
                url: "{{route('admin.clients.index')}}",
                type: "GET",
                data: {search_client: $(this).val()},
                success: function (res) {
                    $('#search-result').html(res);
                }
            });
        });
	</script>
@endsection
