@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>{{ $title }}</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>{{ $title }}</h1>
		</div>
		<div class="clearfix"></div>
		@if (isset($client))
			<form action="{{ route('admin.clients.edit_p', ['client_id'=>$client->id]) }}" method="post">
		@else
			<form action="{{ route('admin.clients.add_p') }}" method="post">
		@endif
			@csrf
			<div class="row">
				<div class="col-md-12">
					@if (isset($errors))
						@foreach($errors->all() as $error)
							<div class="alert alert-warning">{{ $error }}</div>
						@endforeach
					@endif
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="user_sur">Фамилия</label>
						<input id="user_sur" type="text" name="surname" value="@if (isset($client)) {{ $client->surname }} @else {{old('surname')}}@endif" class="form-control">
					</div>
					<div class="form-group">
						<label for="user_name">Имя</label>
						<input id="user_name" type="text" name="name" value="@if (isset($client)) {{ $client->name }} @else {{old('name')}}@endif" class="form-control">
					</div>
					<div class="form-group">
						<label for="user_dob">Дата рождения ДД.ММ.ГГГГ</label>
						<input id="user_dob" type="text" name="birthdate" value="@if (isset($client)){{ date('d.m.Y',strtotime($client->birthdate)) }} @else {{old('birthdate')}}@endif" class="form-control">
					</div>
					<div class="form-group">
						<label for="user_sex">Пол</label>
						<select id="user_sex" name="sex_id" class="form-control">
							<option value="">Выберите</option>
							<option value="1" @if (isset($client)) @if ($client->sex_id == 1) selected @endif @endif>Мужской</option>
							<option value="2" @if (isset($client)) @if ($client->sex_id == 2) selected @endif @endif>Женский</option>
						</select> 
					</div>
					<div class="form-group">
						<label for="user_phone">Телефон</label>
						<input id="user_phone" type="text" name="phone" value="@if (isset($client)) {{ $client->phone }} @else {{old('phone')}}@endif" class="form-control">
					</div>

				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="user_email">E-mail</label>
						<input id="user_email" type="email" name="email" value="@if (isset($client)) {{ $client->email }} @else {{old('email')}}@endif" class="form-control">
					</div>
					<div class="form-group">
						<label for="user_growth">Рост</label>
						<input id="user_growth" type="text" name="growth" value="@if (isset($client)) {{ $client->growth }} @else {{old('growth')}}@endif" class="form-control">
					</div>
					<div class="form-group">
						<label for="user_weight">Вес</label>
						<input id="user_weight" type="text" name="weight" value="@if (isset($client)) {{ $client->weight }} @else {{old('weight')}}@endif" class="form-control">
					</div>
					<div class="form-group">
						<label for="user_ver">Аккаунт подтвержден (верификация)</label>
						<select id="user_ver" name="verificated" class="form-control">
							<option value="0" @if (isset($client)) @if ($client->verificated == 0) selected @endif @endif>Нет</option>
							<option value="1" @if (isset($client)) @if ($client->verificated == 1) selected @endif @endif>Да</option>
						</select> 	
					</div>
					<div class="form-group">
						<label for="user_lock">Аккаунт заблокирован</label>
						<select id="user_lock" name="is_locked" class="form-control">
							<option value="0" @if (isset($client)) @if ($client->is_locked == 0) selected @endif @endif>Нет</option>
							<option value="1" @if (isset($client)) @if ($client->is_locked == 1) selected @endif @endif>Да</option>
						</select> 
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label for="user_memo">Дополнительная информация</label>
					<textarea id="user_memo" name="additional" rows="5" class="form-control">@if (isset($client)) {{ $client->additional }} @else {{old('additional')}}@endif</textarea>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<br>
					<div class="form-group">
						<input type="submit" value="{{ $title }}" class="form-control btn btn-primary">
					</div>		 			
				</div>
			</div>
			@if (isset($client))
				</form>
			@else
				</form>
		@endif
	</div>
@endsection