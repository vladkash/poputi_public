<!DOCTYPE html>
<html>
<head> 
	<meta charset="utf-8">
	<title>ПоПути</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body class="login">
	<div class="container-fluid">
		<a href="index.html" class="logo"></a>
		<div class="form_signin">
			<form action="" method="post">
				<h2>Вход в админ панель</h2>
				@if (isset($errors))
					@foreach($errors->all() as $error)
						<div class="alert alert-warning">{{ $error }}</div>
					@endforeach
				@endif
				@csrf
				<input type="text" name="username" placeholder="Ваш логин">
				<input type="password" name="password" placeholder="Введите пароль">
				<input type="submit" name="login" value="Войти">
			</form>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>