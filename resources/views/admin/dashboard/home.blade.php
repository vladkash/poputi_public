@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<h1>Dashboard</h1>
		<ul class="top_menu">
			<li>Всех велосипедов<span>{{$count_bikes}}</span></li>
			<li>Все парковки<span>{{$count_parkings}}</span></li>
			<li>Все пользователи<span>{{$count_users}}</span></li>
			<li>Неисправные велосипеды<span>{{$count_break_bikes}}</span></li>
			<li>Велосипеды на линии<span>{{$count_working_bikes}}</span></li>
		</ul>
		<div class="clearfix"></div>
		<br><br>
		<div class="row" style="display: flex; align-items: baseline">
			<div class="col-md-3">
				<h2>Что сделано за</h2>
			</div>
			<div class="col-md-4">
				<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						{{$title_date}}
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li><a href="{{route('admin.dashboard',['date'=>'day'])}}">День</a></li>
						<li><a href="{{route('admin.dashboard',['date'=>'week'])}}">Неделю</a></li>
						<li><a href="{{route('admin.dashboard',['date'=>'month'])}}">Месяц</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="{{route('admin.dashboard')}}">Все время</a></li>
					</ul>
				</div>
			</div>
		</div>


		<ul class="top_menu">
			<li>Новых пользователей<span>{{$date_new_users}}</span></li>
			<li>Внесенных залогов<span>{{$date_new_in_pledges}}</span></li>
			<li>Внесенных депозитов<span>{{$date_new_payments}}</span></li>
			<li>Возвращенных залогов<span>{{$date_new_out_pledges}}</span></li>
		</ul>
	</div>
@endsection