@extends("admin.layouts.layout")
@section("content")
	<div class="container">

		<h1>{{ $ticket->subject }}</h1>
		<div class="dropdown">
			<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				{{DB::table('ticket_status')->where('id',$ticket->status_id)->first()->name}}
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				@foreach(DB::table('ticket_status')->get() as $status)
					<li><a href="{{route('admin.tickets.changestatus',['ticket_id'=>$ticket->id, 'status_id'=>$status->id])}}">{{$status->name}}</a></li>
				@endforeach
			</ul>
		</div>
		<div class="row">
		  <div class="col-md-8">
		  	<p>
		  		<span class="glyphicon glyphicon-time" aria-hidden="true"></span> {{ date('H:i d.m.Y', strtotime($ticket->created_at)) }}
		  	</p>
		  	<p>
		  		<strong>Автор:</strong>
		  		<a href="{{route('admin.clients.info',['client_id'=>$ticket->client->id])}}">
		  			@if ($ticket->client->username)
							{{ $ticket->client->username }}
						@else
							{{ $ticket->client->id }}
						@endif
		  		</a>
		  	</p>
		  	<p>
		  		<strong>Сообщение:</strong><br>
		  		{{ $ticket->text }}
		  	</p>

		  	<div style="margin-top: 45px;">
		  		<h2>Сообщения тикета</h2>

		  		@if (!$ticket->messages)
		  			Сообщений нет
		  		@else
		  			@foreach ($messages as $message)
		  			<div style="margin-top: 35px;">
		  				<p>
					  		<strong>Отправитель:</strong>
							@if($message->is_admin)
								Администратор
							@else
								@if ($ticket->client->username)
									{{ $ticket->client->username }}
								@else
									{{ $ticket->client->id }}
								@endif
							@endif

					  	</p>
					  	<p>
					  		{{ $message->text }}
					  	</p>
					  </div>
		  			@endforeach
					{{$messages->links('admin.layouts.pagination')}}
		  		@endif
		  	</div>
		  </div>
		  <div class="col-md-4">
		  	<form action="{{route('admin.tickets.send_message',['ticket_id'=>$ticket->id])}}" method="POST">
				@csrf
				<div class="form-group">
				    <label for="ticket_answer">Ответить в тикет</label>
				    <textarea id="ticket_answer" class="form-control" rows="5" name="text"></textarea>
				  </div>
				  <button type="submit" class="btn btn-default">Отправить</button>
				</form>
		  </div>
		</div>
	</div>
@endsection