<table class="table">
    <thead>
    <tr>
        <th scope="col">Автор</th>
        <th scope="col">Тема</th>
        <th scope="col">Статус</th>
        <th scope="col">Дата</th>
        <th scope="col">Другое</th>
    </tr>
    </thead>
    <tbody id="search-table">
    @foreach ($tickets as $ticket)
        <tr>
            <td><a href="{{route('admin.clients.info',['client_id'=>$ticket->client->id])}}">
                    @if (!empty($ticket->client->username))
                        {{ $ticket->client->username }}
                    @else
                        {{ $ticket->client->id }}
                    @endif
                </a></td>
            <td><a href="{{route('admin.tickets.info',['ticket_id'=>$ticket->id])}}">{{ $ticket->subject }}</a></td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        {{DB::table('ticket_status')->where('id',$ticket->status_id)->first()->name}}
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        @foreach(DB::table('ticket_status')->get() as $status)
                            <li><a href="{{route('admin.tickets.changestatus',['ticket_id'=>$ticket->id, 'status_id'=>$status->id])}}">{{$status->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </td>
            <td>{{ date('H:i d.m.Y', strtotime($ticket->created_at)) }}</td>
            <td>
                @if($ticket->messages?$ticket->messages()->where('is_admin', 1)->first():false)
                @else
                    <span class="label label-success">Тикет без ответа</span>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>