@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<h1>Тикеты</h1>
		
		<div class="container" style="margin-top: 20px; padding: 0px;">
			<input id="search" class="form-control input-lg" type="text" placeholder="Начните вводить...">
		</div>
		<div id="search-result">
			@include("admin.tickets.table")
		</div>
		{{$tickets->links('admin.layouts.pagination')}}
	</div>
@endsection
@section("js")
	<script type="text/javascript">
        $('#search').on('input change', function (e) {
            e.preventDefault();
            $.ajax({
                url: "{{route('admin.tickets.index')}}",
                type: "GET",
                data: {search_ticket: $(this).val()},
                success: function (res) {
                    $('#search-result').html(res);
                }
            });
        });
	</script>
@endsection