@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Тарифы</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>Тарифы</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@if (session('message'))
					<div class="alert alert-warning">{{ session('message') }}</div>
				@endif
			</div>
		</div>
		<div class="black_block">
			<p>Редактирование суммы залога</p>
			<form action="{{route('admin.tarifs.change',['tarif_id'=>8])}}" class="hor" method="post">
				@csrf
				<input type="text" name="price" value="{{\App\Tarif::find(8)->price}} руб.">
				<input type="submit" value="Ok">
			</form>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="gform">
					<h4>Поминутная тарификация</h4>
					<form action="{{route('admin.tarifs.change',['tarif_id'=>1])}}" class="hor" method="post">
						@csrf
						<p>От 1 до 120 минут</p>
						<div class="ff">
							<input type="text" name="price" value="{{\App\Tarif::find(1)->price}} руб.">
							<input type="submit" value="Ok">
						</div>
					</form>
					<form action="{{route('admin.tarifs.change',['tarif_id'=>2])}}" class="hor" method="post">
						@csrf
						<p>От 121 до 240 минут</p>
						<div class="ff">
							<input type="text" name="price" value="{{\App\Tarif::find(2)->price}} руб.">
							<input type="submit" value="Ok">
						</div>
					</form>
					<form action="{{route('admin.tarifs.change',['tarif_id'=>3])}}" class="hor" method="post">
						@csrf
						<p>От 241 до 1440 минут</p>
						<div class="ff">
							<input type="text" name="price" value="{{\App\Tarif::find(3)->price}} руб.">
							<input type="submit" value="Ok">
						</div>
					</form>
					<form action="{{route('admin.tarifs.change',['tarif_id'=>4])}}" class="hor" method="post">
						@csrf
						<p>Ожидание 1 минута</p>
						<div class="ff">
							<input type="text" name="price" value="{{\App\Tarif::find(4)->price}} руб.">
							<input type="submit" value="Ok">
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6">
				<div class="gform">
					<h4>Предоплаченный тариф</h4>
					<form action="{{route('admin.tarifs.change',['tarif_id'=>5])}}" class="hor" method="post">
						@csrf
						<p>Сутки 1440 минут</p>
						<div class="ff">
							<input type="text" name="price" value="{{\App\Tarif::find(5)->price}} руб.">
							<input type="submit" value="Ok">
						</div>
					</form>
					<form action="{{route('admin.tarifs.change',['tarif_id'=>7])}}" class="hor" method="post">
						@csrf
						<p>УИКЕНД (с 00:01 сб до 23:59 вс)</p>
						<div class="ff">
							<input type="text" name="price" value="{{\App\Tarif::find(7)->price}} руб.">
							<input type="submit" value="Ok">
						</div>
					</form>
					<form  action="{{route('admin.tarifs.change',['tarif_id'=>6])}}" class="hor" method="post">
						@csrf
						<p>Неделя 10080 минут</p>
						<div class="ff">
							<input type="text" name="price" value="{{\App\Tarif::find(6)->price}} руб.">
							<input type="submit" value="Ok">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection