@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>{{ $title }}</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>{{ $title }}</h1>
		</div>
		<div class="clearfix"></div>
		@if (isset($bike))
			<form action="{{ route('admin.bikes.edit_p',['bike_id'=>$bike->id]) }}" method="post">
		@else
			<form action="{{ route('admin.bikes.add_p') }}" method="post">
		@endif
             @csrf
			<div class="col-md-6" style="padding: 0;">
				@if (isset($errors))
                    @foreach($errors->all() as $error)
					    <div class="alert alert-warning">{{ $error }}</div>
                    @endforeach
				@endif
				<div class="form-group">
					<label for="bike_id">ID велосипеда</label>
					<input id="bike_id" type="text" name="serial_num" value="@if(isset($bike)){{ $bike->serial_num }} @else {{old('serial_num')}}@endif" class="form-control">
				</div>
				<div class="form-group">
					<label for="bike_id">№ Sim карты</label>
					<input id="bike_id" type="text" name="sim_num" placeholder="+79991234567 " value="@if(isset($bike)){{ $bike->sim_num }}@else{{old('sim_num')}}@endif" class="form-control">
				</div>
					<div class="form-group">
						<label for="bike_id">IMEI</label>
						<input id="bike_id" type="text" name="imei" value="@if(isset($bike)){{ $bike->imei }}@else{{old('imei')}}@endif" class="form-control">
					</div>
				<div class="form-group">
					<label for="parking_id">ID парковки</label>
					<input id="parking_id" type="text" name="parking_id" value="@if(isset($bike)){{ $bike->parking->id }}@else{{old('parking_id')}}@endif" class="form-control">
				</div>
				<div class="form-group">
					<label for="bike_type">Тип велосипеда</label>
					<select id="bike_type" name="type_id" class="form-control">
						<option value="">Выберите</option>
						@foreach ($types as $bt)
							<option value="{{ $bt->id }}" @if (isset($bike)) @if ($bike->type_id == $bt->id) selected @endif @endif>{{ $bt->name }}</option>
						@endforeach
					</select> 
				</div>
				<div class="form-group">
					<label for="bike_model">Модель велосипеда</label>
					<select id="bike_model" name="model_id" class="form-control">
						<option value="">Выберите</option>
						@foreach ($models as $bt)
							<option value="{{ $bt->id }}" @if (isset($bike)) @if ($bike->model_id == $bt->id) selected @endif @endif>{{ $bt->name }}</option>
						@endforeach
					</select> 
				</div>
				<div class="form-group">
					<label for="bike_status">Статус велосипеда</label>
					<select id="bike_status" name="status_id" class="form-control">
						<option value="">Выберите</option>
						@foreach ($status as $bt)
							<option value="{{ $bt->id }}" @if (isset($bike)) @if ($bike->status_id == $bt->id) selected @endif @endif>{{ $bt->name }}</option>
						@endforeach
					</select> 
				</div>
				<div class="form-group">
					<input type="submit" value="{{ $title }}" class="form-control btn btn-primary">
					@if (isset($bike))
						<a href="{{route('admin.bikes.delete',['bike_id'=>$bike->id])}}" class="form-control btn btn-danger">Удалить велосипед</a>
					@endif
				</div>
			</div>
			@if (isset($bike))
			<div class="col-md-6" style="padding-top: 25px;">
				<div class="alert alert-info">Местоположение велосипеда</div>
				<div id="velo_map" style="height: 370px;"></div>
				<div class="col-md-6" style="padding-top:20px">
					<div class="form-group">
						<label for="parking_lat">Широта</label>
						<input id="parking_lat" type="text" name="latitude" value="@if (isset($bike)) {{ $bike->latitude }} @endif" class="form-control">
					</div>
				</div>
				<div class="col-md-6" style="padding-top:20px">
					<div class="form-group">
						<label for="parking_lng">Долгота</label>
						<input id="parking_lng" type="text" name="longitude" value="@if (isset($bike)) {{ $bike->longitude }} @endif" class="form-control">
					</div>
				</div>
			</div>
			@endif
				@if (isset($bike))
						</form>
					@else
						</form>
				@endif
	</div>
	<div class="container" style="padding: 0; margin-top: 25px;">
		@include('admin.trips.table')
        @if(isset($bike) && $bike->trips)
		{{$trips->links('admin.layouts.pagination')}}
        @endif
	</div>
    @endsection
	@section("js")
	<script>
		@if(isset($bike))
        ymaps.ready(function () {
            var myMap = new ymaps.Map('velo_map', {
                    center: [parseFloat("{{$bike->latitude}}"),parseFloat("{{$bike->longitude}}")],
                    zoom: 15
                }, {
                    searchControlProvider: 'yandex#search'
                }),
                    myPlacemark = new ymaps.Placemark([parseFloat("{{$bike->latitude}}"),parseFloat("{{$bike->longitude}}")], {
                        hintContent: 'Велосипед {{$bike->serial_num}}, {{$bike->lock->locked?'Закрыт':'Открыт'}} {{$bike->lock->voltage}}',
                    }, {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: 'default#image',
                        // Своё изображение иконки метки.
                        iconImageHref: '{{url('/img/bike.png')}}',
                        // Размеры метки.
                        iconImageSize: [70, 100],
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [-35, -85]
                    });
            myMap.geoObjects
                .add(myPlacemark);
        });
        @endif
	</script>
    @endsection
