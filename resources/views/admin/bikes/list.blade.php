@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Велосипеды</li>
		</ul>

	</div>
	<div class="container-fluid map atab_map atab">
		<div class="container">
			<div class="clearfix"></div>
			<div class="header">
				<h1>Велосипеды (карта)</h1>
				<ul>
					<li><a href="#" data-tab="map" class="mode_map rtool active a_tab"></a></li>
					<li><a href="#" data-tab="list" class="mode_table rtool a_tab"></a></li>
				</ul>
			</div>
			<ul class="buttons">
				<li><a href="{{route('admin.bikes.add')}}">Добавить велосипед</a></li>
				<li><a href="{{route('admin.types.index')}}">Типы велосипедов</a></li>
				<li><a href="{{route('admin.models.index')}}">Модели велосипедов</a></li>
			</ul>
			<div class="clearfix"></div>
			<br>
		</div>
		<div id="velo_map"></div>
	</div>
	<div class="container atab_list atab" style="display: none;">
		<div class="clearfix"></div>
		<div class="header">
			<h1>Велосипеды (список)</h1>
			<ul>
				<li><a href="#" data-tab="map" class="mode_map rtool a_tab active"></a></li>
				<li><a href="#" data-tab="list" class="mode_table rtool a_tab"></a></li>
			</ul>
		</div>
		<ul class="buttons">
            <li><a href="{{route('admin.bikes.add')}}">Добавить велосипед</a></li>
            <li><a href="{{route('admin.types.index')}}">Типы велосипедов</a></li>
            <li><a href="{{route('admin.models.index')}}">Модели велосипедов</a></li>
		</ul>
		<div class="clearfix"></div>
		<br>

	<div class="container" style="margin-top: 20px;">
		<input id="search" class="form-control input-lg" type="text" placeholder="Начните вводить...">
	</div>
	<div class="container" style="margin-top: 20px;">
		<div id="search-result">
			@include('admin.bikes.table')
			{{$bikes->links('admin.layouts.pagination')}}
		</div>
	</div>
	</div>
@endsection

@section("js")
	<script>
        $('#search').on('input change', function (e) {
            e.preventDefault();

            $.ajax({
                url: "{{route('admin.bikes.index')}}",
                type: "GET",
                data: {search_velo: $(this).val()},
                success: function (res) {
                    $('#search-result').html(res);
                }
            });
        });
        ymaps.ready(function () {
            var myMap = new ymaps.Map('velo_map', {
                    center: [47.2295101, 39.6762907],
                    zoom: 12
                }, {
                    searchControlProvider: 'yandex#search'
                }),
				@foreach($bikes as $bike)
				@if($bike->latitude && $bike->longitude)
                myPlacemark = new ymaps.Placemark([parseFloat("{{$bike->latitude}}"),parseFloat("{{$bike->longitude}}")], {
                    hintContent: 'Велосипед {{$bike->serial_num}}, {{$bike->lock->locked?'Закрыт':'Открыт'}} {{$bike->lock->voltage}}',
                }, {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: 'default#image',
                    // Своё изображение иконки метки.
                    iconImageHref: '{{url('/img/bike.png')}}',
                    // Размеры метки.
                    iconImageSize: [70, 100],
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [-35, -85]
                });
            myPlacemark.events.add('click',function () {
                window.open('{{route('admin.bikes.edit',['bike_id'=>$bike->id])}}','_blank');
            });
            myMap.geoObjects
                .add(myPlacemark);
			@endif
			@endforeach
        });
        $('#velo_map').css('height', $(document).height()/1.5);
		$(document).ready(function() {
			$('.a_tab').click(function(e) {
				e.preventDefault();
				this_tab = $(this).data('tab');
				this_t = $('.atab_'+this_tab);
				if (!this_t.length) {					
					return false;
				}
				type = $(this).attr('data-tab');
				$('.a_tab').removeClass('active');
				$('.a_tab[data-tab='+type+']').addClass('active');
				$('.atab').fadeOut('fast');
				$(this_t).fadeIn('fast');
			});
		});
	</script>
@endsection