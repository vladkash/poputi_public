<table class="table">
    <thead>
    <tr>
        <th scope="col">№</th>
        <th scope="col">№ SIM</th>
        <th scope="col">Статус</th>
        <th scope="col">GPS</th>
        <th scope="col">Последнее использование</th>
    </tr>
    </thead>
    <tbody id="search-table">
    @if(isset($bikes))
    @foreach ($bikes as $bike)
        <tr class="{{ $bike->class }}">
            <td scope="row"><a href="{{route('admin.bikes.edit',['bike_id'=>$bike->id])}}">{{ $bike->serial_num }}</a></td>
            <td scope="row">{{ $bike->sim_num }}</td>
            <td>{{ $bike->status }}</td>
            <td>
                @if (isset($bike->latitude))
                    {{ $bike->latitude }}, {{ $bike->longitude }}
                @else
                    не указано
                @endif
            </td>
            <td>
                @if($bike->last_client)
                    <a target="_blank" href="{{route('admin.clients.info',['client_id'=>$bike->last_client->id])}}">{{$bike->last_client->surname?$bike->last_client->surname.' '.$bike->last_client->name : $bike->last_client->id}}</a> {{date('H:i d.m.Y', strtotime($bike->last_trip->created_at))}}
                @else
                    Нет информации
                @endif
            </td>
        </tr>
    @endforeach
    @else
        <tr><td>Нет информации</td></tr>
    @endif
    </tbody>
</table>