@extends("admin.layouts.layout")

@section("content")
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="{{route('admin.promoactions.index')}}" title="Промокоды">Промокоды</a></li>
            <li>{{ $title }}</li>
        </ul>
        <div class="clearfix"></div>
        <div class="header">
            <h1>{{ $title }}</h1>
        </div>
        <div class="clearfix"></div>
        @if (isset($promoaction))
            <form action="{{ route($route_name.'_p',['promoaction_id'=>$promoaction->id]) }}" method="post">
                @else
                    <form action="{{ route($route_name.'_p') }}" method="post">
                        @endif
                        @csrf
                        <div class="col-md-6" style="padding: 0;">
                            @if (isset($errors))
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-warning">{{ $error }}</div>
                                @endforeach
                            @endif
                            <div class="form-group">
                                <label for="promoaction_name">Имя</label>
                                <input id="promoaction_name" type="text" name="name" value="@if(isset($promoaction)){{ $promoaction->name }}@else{{old('name')}}@endif" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="promoaction_name">Сумма</label>
                                <input id="promoaction_name" type="text" name="amount" value="@if(isset($promoaction)){{ $promoaction->amount }}@else{{old('amount')}}@endif" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="promoaction_name">Код</label>
                                <input id="promoaction_name" type="text" name="code" placeholder="Оставьте пустым для случайной генерации" value="@if(isset($promoaction)){{ $promoaction->code }}@else{{old('code')}}@endif" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="{{ $title }}" class="form-control btn btn-primary">
                                @if(isset($promoaction))
                                    <a href="{{route('admin.promoactions.delete',['promoaction_id'=>$promoaction->id])}}" class="form-control btn btn-danger">Удалить</a>
                                @endif
                            </div>
                        </div>
                        @if(isset($promoaction))
                    </form>
                    @else
            </form>
        @endif
    </div>
@endsection