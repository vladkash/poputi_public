@extends("admin.layouts.layout")

@section("content")
    <div class="container atab_list atab">
        <div class="clearfix"></div>
        <div class="header">
            <h1>Промокоды и промоакции</h1>
        </div>
        <ul class="buttons">
            <li><a href="{{route('admin.promocodes.add')}}">Добавить промокод</a></li>
            <li><a href="{{route('admin.promoactions.add')}}">Добавить промоакцию</a></li>
        </ul>
        <div class="clearfix"></div>
        <br>

        <div class="container" style="margin-top: 20px;">
            <div class="row">
                <div class="col-md-12">
                    @if (session('message'))
                        <div class="alert alert-warning">{{ session('message') }}</div>
                    @endif
                </div>
            </div>
            <div class="">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Код</th>
                        <th scope="col">Сумма</th>
                        <th scope="col">Количество использований</th>
                        <th scope="col">Дата создания</th>
                    </tr>
                    </thead>
                    <tbody id="search-table">
                    @foreach ($promoactions as $promoaction)
                        <tr>
                            <td scope="row">{{$promoaction->id}}</td>
                            <td>{{$promoaction->name}}</td>
                            <td><a href="{{route('admin.promoactions.edit', ['promoaction_id'=>$promoaction->id])}}">{{$promoaction->code}}</a></td>
                            <td>{{$promoaction->amount}}</td>
                            <td>{{$promoaction->clients()->count()}}</td>
                            <td>{{date('H:i d.m.Y', strtotime($promoaction->created_at))}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$promoactions->links('admin.layouts.pagination')}}
            </div>
        </div>
    </div>
@endsection