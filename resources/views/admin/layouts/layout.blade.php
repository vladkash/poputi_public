<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
</head>
<body class="inside">
<nav class="navbar navbar-default navbar-static-top">
    <div class="container" style="width: 1250px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                @include("admin.layouts.menu")
            </ul>
        </div>
    </div>
</nav>
@yield("content")
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/popper.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>

{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBw2B7lsWn4yTLJ3lEYXOjDX0lrRlf-LvQ&callback=initMap"
        async defer></script>--}}
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=10f356f0-a26f-4a62-b590-3ce4832edd3a" type="text/javascript"></script>
@yield("js")
</body>
</html>{{--AIzaSyBw2B7lsWn4yTLJ3lEYXOjDX0lrRlf-LvQ--}}