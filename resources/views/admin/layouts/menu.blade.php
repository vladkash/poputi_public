<ul class="nav navbar-nav">
    <li><a href="{{route('admin.dashboard')}}" @if(strpos($route_name, "dashboard"))class="active"@endif>Dashboard</a></li>
    <li><a href="{{route('admin.bikes.index')}}" @if(strpos($route_name, "bikes") || strpos($route_name, "models") || strpos($route_name, "types"))class="active"@endif>Велосипеды</a></li>
    <li><a href="{{route('admin.parkings.index')}}" @if(strpos($route_name, "parkings"))class="active"@endif>Парковки</a></li>
    <li><a href="{{route('admin.clients.index')}}" @if(strpos($route_name, "clients"))class="active"@endif>Пользователи</a></li>
    <li><a href="{{route('admin.trips.index')}}" @if(strpos($route_name, "trips"))class="active"@endif>Поездки</a></li>
    <li><a href="{{route('admin.payments.index')}}" @if(strpos($route_name, "payments"))class="active"@endif>Финансы</a></li>
    <li><a href="{{route('admin.tickets.index')}}" @if(strpos($route_name, "tickets"))class="active"@endif>Тикеты</a></li>
    <li><a href="{{route('admin.promoactions.index')}}" @if(strpos($route_name, "promoactions") || strpos($route_name, "promocodes"))class="active"@endif>Промокоды</a></li>
    <li><a href="{{route('admin.tarifs.index')}}" @if(strpos($route_name, "tarifs"))class="active"@endif>Тарифы</a></li>
    <li class="lower"> <form id="logout-form" action="{{route('logout')}}" method="post">@csrf</form><a href="{{ route('logout') }}" onclick="
        event.preventDefault();
        document.getElementById('logout-form').submit();">Выход</a></li>
</ul>