<table class="table">
    <thead>
    <tr>
        <th scope="col">ID пользователя</th>
        <th scope="col">Дата время</th>
        <th scope="col">Способ оплаты</th>
        <th scope="col">Сумма транзакции</th>
        <th scope="col">Тип</th>
        <th scope="col">Статус</th>
    </tr>
    </thead>
    <tbody>
    @foreach($payments as $payment)
        <tr>
            <th scope="row"><a href="{{route('admin.clients.info',['client_id'=>$payment->client->id])}}">{{$payment->client->id}}</a></th>
            <td>{{date('H:i',strtotime($payment->created_at))}}</td>
            <td>{{$payment->method}}</td>
            <td>{{$payment->amount}} руб.</td>
            <td>{{DB::table('paymenttype')->where('id',$payment->type_id)->first()->name}}</td>
            <td>{{DB::table('payment_status')->where('id',$payment->status_id)->first()->name}}</td>
        </tr>
    @endforeach
    </tbody>
</table>