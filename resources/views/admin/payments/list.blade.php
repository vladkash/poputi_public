@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Финансы</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>Финансовые операции</h1>
		</div>
	</div>
	<div class="container">
		@include('admin.payments.table')
		{{$payments->links('admin.layouts.pagination')}}
	</div>
@endsection
@section("js")
	<script type="text/javascript">
        $('#search').on('input change', function (e) {
            e.preventDefault();

            $.ajax({
                url: "/admin",
                type: "GET",
                data: {search_trips: $(this).val()},
                success: function (res) {
                    $('#search-table').html(res);
                }
            });
        });
	</script>
@endsection