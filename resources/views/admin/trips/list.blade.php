@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Поездки</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>Поездки пользователей</h1>
		</div>
		<div style="margin-top: 20px; ">
			<input id="search" class="form-control input-lg" type="text" placeholder="Начните вводить...">
		</div>
	</div>
	<div class="container">
		@include("admin.trips.table")
		{{$trips->links('admin.layouts.pagination')}}
	</div>
@endsection
@section("js")
	<script type="text/javascript">
        $('#search').on('input change', function (e) {
            e.preventDefault();

            $.ajax({
                url: "{{route('admin.trips.index')}}",
                type: "GET",
                data: {search_trips: $(this).val()},
                success: function (res) {
                    $('#search-table').html(res);
                }
            });
        });
	</script>
@endsection