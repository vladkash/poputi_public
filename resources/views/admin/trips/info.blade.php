@extends("admin.layouts.layout")
@section("content")
	<div class="container">
		<ul class="breadcrumbs">
			<li><a href="{{route('admin.dashboard')}}" title="Главная">Главная</a></li>
			<li>Карточка поездки</li>
		</ul>
		<div class="clearfix"></div>
		<div class="header">
			<h1>Карточка поездки</h1>
		</div>
	</div>
	<div class="container">
		<div class="atab atab_info">
			<div class="row user">
				<div class="col-md-12">
					<table class="trip">
					<tr>
						<td>ID поездки</td>
						<td>{{ $trip->id }}</td>
					</tr>
					<tr>
						<td>ID пользователя</td>
						<td><a href="{{route('admin.clients.edit', ['client_id'=>$trip->client->id])}}">{{$trip->client->id}}</a></td>
					</tr>
					<tr>
						<td>Время начала</td>
						<td>{{date('H:i d.m.Y',strtotime($trip->created_at))}}</td>
					</tr>
					<tr>
						<td>GPS начала</td>
						<td>
							{{$trip->history()->oldest()->first()->latitude}}
							<br>
							{{$trip->history()->oldest()->first()->longitude}}
						</td>
					</tr>
					<tr>
						<td>Время окончания</td>
						<td>@if($trip->done)
								{{date('H:i d.m.Y',strtotime($trip->updated_at))}}
							@else
								Поездка не окончена
							@endif</td>
					</tr>
					<tr>
						<td>GPS окончания</td>
						<td>@if($trip->done)
								{{$trip->history()->latest()->first()->latitude}}
								<br>
								{{$trip->history()->latest()->first()->longitude}}
							@else
								Поездка не окончена
							@endif</td>
					</tr>
					<tr>
						<td>Статус</td>
						<td>
							@if($trip->done)
								Завершена
							@else
								Поездка не окончена
							@endif
						</td>
					</tr>
					<tr>
						<td>Цена</td>
						<td>
							@if($trip->done)
								{{$trip->price}} руб.
							@else
								Поездка не окончена
							@endif
						</td>
					</tr>
					<tr>
						<td class="orange" colspan="2">Трек на карте</td>
					</tr>
					<tr>
						<td colspan="2">
							<div id="velo_map" style="height: 750px;"></div>
						</td>
					</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
	@section("js")
	<script>
		var coordinates = [];
		@foreach($trip->coordinates_history as $coordinates)
			coordinates.push([parseFloat("{{$coordinates->latitude}}"), parseFloat("{{$coordinates->longitude}}")]);
		@endforeach
        ymaps.ready(function () {
            var myMap = new ymaps.Map('velo_map', {
                    center: [47.2295101, 39.6762907],
                    zoom: 12
                }, {
                    searchControlProvider: 'yandex#search'
                });
            ymaps.route(coordinates).then(
                function (route) {
                    myMap.geoObjects.add(route);
                },
                function (error) {
                    alert('Возникла ошибка: ' + error.message);
                }
            );
        });
	</script>
@endsection
