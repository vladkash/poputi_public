<table class="table">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">ID Вел.</th>
        <th scope="col">ID Пол.</th>
        <th scope="col">Статус</th>
        <th scope="col">Время начала</th>
        <th scope="col">GPS начало</th>
        <th scope="col">Время окончания</th>
        <th scope="col">GPS окончание</th>
        <th scope="col">Тариф</th>
        <th scope="col">Цена</th>
    </tr>
    </thead>
    <tbody id="search-table">
    @if(isset($trips) && !empty($trips))
    @foreach($trips as $trip)
        <tr>
            <td><a href="{{route('admin.trips.info',['trip_id'=>$trip->id])}}">{{$trip->id}}</a></td>
            <td>@if($trip->bike)<a href="{{route('admin.bikes.edit',['bike_id'=>$trip->bike->id])}}">{{$trip->bike->id}}</a>@else Удален@endif</td>
            <td>@if($trip->client)<a href="{{route('admin.clients.info',['client_id'=>$trip->client->id])}}">{{$trip->client->id}}</a>@else Удален@endif</td>
            <td>{{$trip->done? 'Окончена':'В процессе'}}</td>
            <td>{{date('H:i d.m.Y',strtotime($trip->created_at))}}</td>
            <td>{{$trip->history()->oldest()->first()->latitude}}
                <br>
                {{$trip->history()->oldest()->first()->longitude}}</td>
            <td>
                @if($trip->done)
                    {{date('H:i d.m.Y',strtotime($trip->updated_at))}}
                @else
                    Поездка не окончена
                @endif
            </td>
            <td>
                @if($trip->done)
                    {{$trip->history()->latest()->first()->latitude}}
                    <br>
                    {{$trip->history()->latest()->first()->longitude}}
                @else
                    Поездка не окончена
                @endif
            </td>
            <td>{{$trip->tarif_type->name}}</td>
            <td>{{$trip->price}}</td>
        </tr>
    @endforeach
    @else
        <tr><td>Пусто</td></tr>
    @endif
    </tbody>
</table>