<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Client extends Authenticatable
{
    use Notifiable, SoftDeletes, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    protected $hidden = [
        'vote_type',
        'email_verified_at',
        'is_vote',
        'additional'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    public function trips()
    {
        return $this->hasMany('App\Trip');
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function promocodes()
    {
        return $this->hasMany('App\Promocode');
    }

    public function verification()
    {
        return $this->hasOne('App\Verification');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function promoactions()
    {
        return $this->belongsToMany('App\Promoaction');
    }

    public function pledge()
    {
        return $this->hasOne('App\Pledge');
    }
}
