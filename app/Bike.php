<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bike extends Model
{
    protected $guarded = [];

    protected $hidden = ['sim_num'];
    public function parking()
    {
        return $this->belongsTo('App\Parking');
    }

    public function trips()
    {
        return $this->hasMany('App\Trip');
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }

    public function lock()
    {
        return $this->belongsTo('App\Lock');
    }
}
