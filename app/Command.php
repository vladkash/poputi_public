<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    protected $guarded = [];

    public function lock()
    {
        return $this->belongsTo('App\Lock');
    }
}
