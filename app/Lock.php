<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lock extends Model
{
    protected $guarded = [];

    protected $hidden = ['resource_id','imei','info_serial_num'];

    public function bike()
    {
        return $this->HasOne('App\Bike');
    }

    public function command()
    {
        return $this->hasOne('App\Command');
    }
}
