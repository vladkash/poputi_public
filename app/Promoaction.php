<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promoaction extends Model
{
    protected $guarded = [];

    public function clients()
    {
        return $this->belongsToMany('App\Client');
    }
}
