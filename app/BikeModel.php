<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BikeModel extends Model
{
    protected $guarded = [];

    public function bikes()
    {
        return $this->hasMany('App\Bike');
    }
}
