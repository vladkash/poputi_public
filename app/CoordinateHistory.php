<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoordinateHistory extends Model
{
    protected $guarded = [];

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }
}
