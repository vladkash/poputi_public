<?php

namespace App\Jobs;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class StopBooking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $booking;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //save booking done status
        $this->booking->done = 1;
        $this->booking->save();
        //save bike status
        $bike = $this->booking->bike;
        $bike->status_id = 1;
        $bike->save();
        //sub price of booking from client balance
        $this->booking->client->balance -= $this->booking->price;
        $this->booking->client->save();
    }
}
