<?php

namespace App\Jobs;

use App\Lock;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetCoordinates implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $lock;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Lock $lock)
    {
        $this->lock = $lock;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->lock->command()->create(['command'=>'GPSON#']);
    }
}
