<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
