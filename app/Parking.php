<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    protected $guarded = [];

    public function bikes()
    {
        return $this->hasMany('App\Bike');
    }
}
