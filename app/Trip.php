<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $guarded = [];

    public function history()
    {
        return $this->hasMany('App\BikeHistory');
    }

    public function bike()
    {
        return $this->belongsTo('App\Bike');
    }

    public function tarif_type()
    {
        return $this->belongsTo('App\TarifType');
    }

    public function coordinates_history()
    {
        return $this->hasMany('App\CoordinateHistory');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}