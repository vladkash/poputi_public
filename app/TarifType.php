<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TarifType extends Model
{
    protected $guarded = [];

    public  function tarifs()
    {
        return $this->hasMany('App\Tarif');
    }
}
