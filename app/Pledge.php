<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pledge extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function client ()
    {
        return $this->belongsTo('App\Client');
    }
}
