<?php

namespace App\Http\Controllers\API;

use App\TarifType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TarifController extends Controller
{
    public function getTarifs()
    {
        $tarif_types = TarifType::all();
        foreach ($tarif_types as $tarif_type) {
            $tarifs = $tarif_type->tarifs;
            $tarif_type->tarifs = $tarifs;
        }
        $response = new ApiResponse(true, $tarif_types);
        return response()->json($response, 200);
    }
}
