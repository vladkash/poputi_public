<?php

namespace App\Http\Controllers\API;

use App\Bike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BikeController extends Controller
{
    public function getBikes()
    {
        $bikes = Bike::where('status_id', 1)->get();
        $response = new ApiResponse(true, $bikes);
        return response()->json($response, 200);
    }
}
