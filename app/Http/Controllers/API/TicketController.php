<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    private $set_visible_message = false;
    private $messages;


    public function getTickets()
    {
        $client = Auth::user();
        $tickets = $client->tickets;
        foreach ($tickets as $ticket) {
            $last_message = $ticket->messages()->latest()->first();
            if ($last_message) {
                $ticket->last_message = $last_message;
            } else {
                $ticket->last_message = null;
            }

        }
        $response = new ApiResponse(true, $tickets);
        return response()->json($response, 200);
    }

    public function getMessages(Request $request)
    {
        $validation = [
            'ticket_id'=>'required|exists:tickets,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        $ticket = $client->tickets()->find($data['ticket_id']);
        if (!$ticket) {
            $response = new ApiResponse(false, null, 'Тикет не найден!');
            return response()->json($response, 200);
        }
        $messages = $ticket->messages;
        $this->set_visible_message = true;
        $this->messages = $messages;
        $response = new ApiResponse(true, $messages);
        return response()->json($response, 200);
    }

    public function addMessage(Request $request)
    {
        $validation = [
            'text'=>'required',
            'ticket_id'=>'required|exists:tickets,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        $ticket = $client->tickets()->find($data['ticket_id']);
        if (!$ticket) {
            $response = new ApiResponse(false, null, 'Тикет не найден!');
            return response()->json($response, 200);
        }
        $message = $ticket->messages()->create([
            'text'=>$data['text']
        ]);
        $response = new ApiResponse(true, $message);
        return response()->json($response, 200);
    }

    public function addTicket(Request $request)
    {
        $validation = [
            'subject'=>'required',
            'text'=>'required'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        $ticket = $client->tickets()->create([
            'subject'=>$data['subject'],
            'text'=>$data['text'],
        ]);
        $response = new ApiResponse(true, $ticket);
        return response()->json($response, 200);
    }

    public function __destruct()
    {
        if ($this->set_visible_message) {
            foreach ($this->messages as $message) {
                if ($message->is_admin) {
                    $message->viewed = 1;
                    $message->save();
                }
            }
        }
    }
}
