<?php

namespace App\Http\Controllers\API;

use App\SmsCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Client;

class AuthController extends Controller
{
    public function sendSms(Request $request)
    {
        $validation = [
          'phone'=>'required'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $code = rand(1001, 9999);
        //write code in db
        $code_db = SmsCode::where('phone', $data['phone'])->first();
        if ($code_db) {
            $code_db->code = $code;
            $code_db->save();
        }else{
            SmsCode::create([
                'phone'=>$data['phone'],
                'code'=>$code
            ]);
        }
        //send sms
        $msg = 'PoPuti phone code: '.$code;
        $url = 'https://smsc.ru/sys/send.php?login='.env('SMSC_LOGIN').'&psw='.env('SMSC_PASSWORD').'&phones='.$data['phone'].'&sender=Poputi&mes='.$msg;
        $resp = file_get_contents($url);
        $response = new ApiResponse(true, null, $resp);
        return response()->json($response, 200);
    }

    public function login(Request $request)
    {
        $validation = [
            'phone'=>'required|exists:sms_codes',
            'code'=>'required'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        //check code
        $sms_code = SmsCode::where('phone', $data['phone'])->first();
        if ($sms_code->code != $data['code']) {
            $response = new ApiResponse(false, null,'Неверный код из sms!');
            return response()->json($response, 200);
        }
        $sms_code->delete();
        $client = Client::where('phone',$data['phone'])->first();
        //if client not found
        if (!$client) {
            $response = new ApiResponse(false, null,'Необходимо зарегистрироваться!');
            return response()->json($response, 200);
        }
        $client->lastvisit = date('Y-m-d H:i:s');
        $client->save();
        $client->token = $client->createToken('MyApp')->accessToken;
        $response = new ApiResponse(true, $client);
        return response()->json($response, 200);
    }

    public function register(Request $request)
    {
        $validation = [
            'phone'=>'required|exists:sms_codes|unique:clients',
            'code'=>'required'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        //check code
        $sms_code = SmsCode::where('phone', $data['phone'])->first();
        if ($sms_code->code != $data['code']) {
            $response = new ApiResponse(false, null,'Неверный код из sms!');
            return response()->json($response, 200);
        }
        //delete last code, create client
        $sms_code->delete();
        $client = Client::create([
            'phone'=>$data['phone']
        ]);
        $client->lastvisit = date('Y-m-d H:i:s');
        $client->save();
        $client->token = $client->createToken('MyApp')->accessToken;
        $response = new ApiResponse(true, $client);
        return response()->json($response, 200);
    }
}
