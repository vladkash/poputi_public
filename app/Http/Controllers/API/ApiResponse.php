<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiResponse extends Controller
{
    public $data;
    public $message;
    public $success;
    public $errors;

    public function __construct($success, $data, $message = null, $errors = null)
    {
        $this->data = $data;
        $this->message = $message;
        $this->success = $success;
        $this->errors = $errors;
    }
}
