<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Parking;

class ParkingController extends Controller
{
    public function getParkings()
    {
        $parkings = Parking::where('is_work', 1)->get();
        $response = new ApiResponse(true, $parkings);
        return response()->json($response, 200);
    }
}
