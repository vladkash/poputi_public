<?php

namespace App\Http\Controllers\API;

use App\Tarif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PledgeController extends Controller
{
    public function makePledge()
    {
        $client = Auth::user();
        if ($client->pledge) {
            $response = new ApiResponse(false, null, 'Залог уже внесен!');
            return response()->json($response, 200);
        }
        $amount = Tarif::find(8)->price;
        if ($client->balance < $amount) {
            $response = new ApiResponse(false, null, 'На аккаунте недостаточно средств!');
            return response()->json($response, 200);
        }
        $client->balance -= $amount;
        $client->save();
        $pledge = $client->pledge()->create(['amount' => $amount]);
        $response = new ApiResponse(true, $pledge);
        return response()->json($response, 200);
    }

    public function returnPledge()
    {
        $client = Auth::user();
        $pledge = $client->pledge;
        if (!$pledge) {
            $response = new ApiResponse(false, null, 'Залог не найден!');
            return response()->json($response, 200);
        }
        $client->balance += $pledge->amount;
        $client->save();
        $pledge->delete();
        $response = new ApiResponse(true, null, 'Залог возрващен!');
        return response()->json($response, 200);
    }
}
