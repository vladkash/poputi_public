<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WaitController extends Controller
{
    public function startWaiting(Request $request)
    {
        $validation = [
            'trip_id'=>'required|exists:trips,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        //check for existing trip
        $trip = $client->trips()->where('done', 0)->find($data['trip_id']);
        if (!$trip) {
            $response = new ApiResponse(false, null, 'Поездка не найдена!');
            return response()->json($response, 200);
        }
        //only minute tarif has waiting
        if ($trip->tarif_type_id != 2) {
            $response = new ApiResponse(false, null, 'Ожидание запрещено на этом тарифе!');
            return response()->json($response, 200);
        }
        if (!$trip->bike->lock->locked) {
            $response = new ApiResponse(false, null, 'Необходимо закрыть замок!');
            return response()->json($response, 200);
        }
        //all is right, save waiting status
        $trip->status_id = 4;
        $trip->save();
        $bike = $trip->bike;
        $bike->status_id = 4;
        $bike->save();
        $trip->history()->create([
            'status_id'=>4,
            'latitude'=>$bike->latitude,
            'longitude'=>$bike->longitude
        ]);
        $response = new ApiResponse(true, null, 'Режим ожидания активирован!');
        return response()->json($response, 200);
    }

    public function stopWaiting(Request $request)
    {
        $validation = [
            'trip_id'=>'required|exists:trips,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        //check for existing trip
        $trip = $client->trips()->where('done', 0)->find($data['trip_id']);
        if (!$trip) {
            $response = new ApiResponse(false, null, 'Поездка не найдена!');
            return response()->json($response, 200);
        }
        //all is right, stop waiting
        $trip->status_id = 3;
        $trip->save();
        $bike = $trip->bike;
        $bike->status_id = 3;
        $bike->save();
        $trip->history()->create([
            'status_id'=>3,
            'latitude'=>$bike->latitude,
            'longitude'=>$bike->longitude
        ]);
        $trip->bike->lock->command()->create(['command'=>'UNLOCK#']);
        $response = new ApiResponse(true, $trip, 'Ожидание окончено!');
        return response()->json($response, 200);
    }
}
