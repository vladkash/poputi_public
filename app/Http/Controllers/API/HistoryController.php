<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function getTrips()
    {
        $client = Auth::user();
        $trips = $client->trips()->where('done', 1)->orderBy('id', 'desc')->get();
        foreach ($trips as $trip) {
            $coordinates_history = $trip->coordinates_history;
            $trip->coordinates_history = $coordinates_history;
            $minutes = 0;
            $last_time = null;
            foreach ($trip->history as $history) {
                if ($last_time) {
                    $ride_time = strtotime($history->created_at) - strtotime($last_time);
                    $minutes += $ride_time/60;
                    $last_time = null;
                }
                if ($history->status_id == 3) {
                    $last_time = $history->created_at;
                }
            }
            $distance = 0;
            foreach ($coordinates_history as $history) {
                if (isset($last_latitude) && isset($last_longitude)) {
                    $current_distance = $this->getDistance($last_latitude, $last_longitude, $history->latitude, $history->longitude);
                    $distance += $current_distance;
                    $last_latitude = $history->latitude;
                    $last_longitude = $history->longitude;
                }else{
                    $last_latitude = $history->latitude;
                    $last_longitude = $history->longitude;
                }
            }
            $trip->distance = round($distance, 2);
            $speed = $distance/($minutes/60);
            $trip->speed = round($speed, 2);
            $calories = ceil($minutes * 5.43);
            $trip->calories = $calories;
            $time = ceil((strtotime($trip->updated_at) - strtotime($trip->created_at))/60);
            $trip->time = $time;
            $bike = $trip->bike;
            $trip->serial_num = $bike->serial_num;
        }
        $response = new ApiResponse(true, $trips);
        return response()->json($response, 200);
    }


    public function getTripDetails(Request $request)
    {
        $validation = [
            'trip_id'=>'required|exists:trips,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        $trip = $client->trips()->find($data['trip_id']);
        if (!$trip) {
            $response = new ApiResponse(false, null, 'Поездка не найдена!');
            return response()->json($response, 200);
        }
        $coordinates_history = $trip->coordinates_history;
        $trip->coordinates_history = $coordinates_history;
        $minutes = 0;
        $last_time = null;
        foreach ($trip->history as $history) {
            if ($last_time) {
                $ride_time = strtotime($history->created_at) - strtotime($last_time);
                $minutes += $ride_time/60;
                $last_time = null;
            }
            if ($history->status_id == 3) {
                $last_time = $history->created_at;
            }
        }
        $distance = 0;
        foreach ($coordinates_history as $history) {
            if (isset($last_latitude) && isset($last_longitude)) {
                $current_distance = $this->getDistance($last_latitude, $last_longitude, $history->latitude, $history->longitude);
                $distance += $current_distance;
                $last_latitude = $history->latitude;
                $last_longitude = $history->longitude;
            }else{
                $last_latitude = $history->latitude;
                $last_longitude = $history->longitude;
            }
        }
        $trip->distance = round($distance, 2);
        $speed = $distance/($minutes/60);
        $trip->speed = round($speed, 2);
        $calories = ceil($minutes * 5.43);
        $trip->calories = $calories;
        $time = ceil((strtotime($trip->updated_at) - strtotime($trip->created_at))/60);
        $trip->time = $time;
        $response = new ApiResponse(true, $trip);
        return response()->json($response, 200);
    }

    private  function getDistance( $latitude1, $longitude1, $latitude2, $longitude2 ) {
        $earth_radius = 6371;

        $dLat = deg2rad( $latitude2 - $latitude1 );
        $dLon = deg2rad( $longitude2 - $longitude1 );

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }
}
