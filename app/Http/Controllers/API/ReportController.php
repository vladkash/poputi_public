<?php

namespace App\Http\Controllers\API;

use App\Bike;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    public function reportBike(Request $request)
    {
        $validation = [
            'serial_num'=>'required|exists:bikes',
            'part_id'=>'exists|bike_parts,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $client = Auth::user();
        $last_report = $client->reports()->whereDate('created_at', '>=', date('Y-m-d H:i:s', strtotime('-5 minutes')))->first();
        if ($last_report) {
            $response = new ApiResponse(false, null, 'Попробуйте через 5 минут!');
            return response()->json($response, 200);
        }
        $data = $request->all();
        $data['type'] = 'bike';
        $bike = Bike::where('serial_num', $data['serial_num'])->first();
        array_forget($data, 'serial_num');
        $data['target_id'] = $bike->id;
        $report = $client->reports()->create($data);
        $response = new ApiResponse(true, $report);
        return response()->json($response, 200);
    }

    public function reportParking(Request $request)
    {
        $validation = [
            'parking_id'=>'required|exists:parkings,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $client = Auth::user();
        $last_report = $client->reports()->whereDate('created_at', '>=', date('Y-m-d H:i:s', strtotime('-5 minutes')))->first();
        if ($last_report) {
            $response = new ApiResponse(false, null, 'Попробуйте через 5 минут!');
            return response()->json($response, 200);
        }
        $data = [];
        $data['type'] = 'parking';
        $data['target_id'] = $request->input('parking_id');
        $report = $client->reports()->create($data);
        $response = new ApiResponse(true, $report);
        return response()->json($response, 200);
    }
}
