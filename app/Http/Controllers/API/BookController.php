<?php

namespace App\Http\Controllers\API;

use App\Bike;
use App\Booking;
use App\Schedule\CalcBookings;
use App\Tarif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function bookBike(Request $request)
    {

        $validation = [
            'serial_num'=>'required|exists:bikes'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        //check for pledge
        if (!$client->pledge) {
            $response = new ApiResponse(false, null, 'Не внесён залог!');
            return response()->json($response, 200);
        }
        //check for verification
        if (!$client->verificated) {
            $response = new ApiResponse(false, null, 'Аккаунт не верифицирован!');
            return response()->json($response, 200);
        }
        //check for existing trips
        $exist_trips = $client->trips()->where('done', 0)->first();
        $exist_bookings = $client->booking()->where('done', 0)->first();
        if ($exist_trips || $exist_bookings) {
            $response = new ApiResponse(false, null, 'Существует незавершенная поездка или бронирование!');
            return response()->json($response, 200);
        }

        //check for free booking
        $last_booking = $client->booking()->latest()->first();
        $is_free = 1;
        if ($last_booking) {
            $time = (strtotime(date('Y-m-d H:i:s')) - strtotime($last_booking->created_at))/60;
            if ($time < 60) {
                $is_free = 0;
            }
        }

        //check bike status
        $bike = Bike::where('serial_num', $data['serial_num'])->first();
        if ($bike->status_id != 1) {
            $response = new ApiResponse(false, null, 'Велосипед недоступен!');
            return response()->json($response, 200);
        }
        $bike->status_id = 2;
        $bike->save();
        $booking = $client->booking()->create([
            'bike_id'=>$bike->id,
            'is_free'=>$is_free
        ]);
        $tarif = Tarif::find(4);
        if ($client->balance < $tarif->price) {
            $response = new ApiResponse(false, null, 'Необходимо пополнить счет!');
            return response()->json($response, 200);
        }
        $response = new ApiResponse(true, $booking);
        return response()->json($response, 200);
    }

    public function cancelBooking(Request $request)
    {
        $validation = [
            'booking_id'=>'required'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        //check booking
        $booking = $client->booking()->where('done', 0)->find($data['booking_id']);
        if (!$booking) {
            $response = new ApiResponse(false, null, 'Бронирование не найдено!');
            return response()->json($response, 200);
        }
        //all is right, wright in db
        $booking->done = 1;
        $booking->save();
        $bike = Bike::find($booking->bike_id);
        $bike->status_id = 1;
        $bike->save();
        CalcBookings::calcBooking($booking->id);
        $client->balance -= $booking->price;
        $client->save();
        $response = new ApiResponse(true, null, 'Бронирование отменено!');
        return response()->json($response, 200);
    }

    public function checkBooking()
    {
        $client = Auth::user();
        $booking = $client->booking()->where('done', 0)->first();
        if ($booking) {
            $bike = $booking->bike;
            $booking->bike = $bike;
        }
        $response = new ApiResponse(true, $booking);
        return response()->json($response, 200);
    }

    public function calcBooking(Request $request)
    {
        $validation = [
            'booking_id'=>'required|exists:bookings,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        CalcBookings::calcBooking($data['booking_id']);
        $booking = Booking::find($data['booking_id']);
        $response = new ApiResponse(true, $booking);
        return response()->json($response, 200);
    }
}
