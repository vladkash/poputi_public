<?php

namespace App\Http\Controllers\API;

use App\Bike;
use App\Http\Controllers\Lock\ServerController;
use App\Http\Controllers\Lock\WebSocketController;
use App\Parking;
use App\Tarif;
use App\TarifType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Schedule\CalcBookings;

class RentController extends Controller
{
    public function rentBike(Request $request)
    {

        $validation = [
            'serial_num'=>'required|exists:bikes',
            'tarif_type_id'=>'required|exists:tarifs',
            'booking_id'=>'exists:bookings,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $bike = Bike::where('serial_num', $data['serial_num'])->first();
        $client = Auth::user();
        //check for pledge
        if (!$client->pledge) {
            $response = new ApiResponse(false, null, 'Не внесён залог!');
            return response()->json($response, 200);
        }
        //check for verification
        if (!$client->verificated) {
            $response = new ApiResponse(false, null, 'Аккаунт не верифицирован!');
            return response()->json($response, 200);
        }
        //check for existing bookings and trips
        $exists_trip = $client->trips()->where('done', 0)->first();
        $exists_booking = $client->booking()->where('done', 0)->first();
        if ($exists_trip || ($exists_booking && $exists_booking->bike_id != $bike->id)) {
            $response = new ApiResponse(false, null, 'Существует незавершенная поездка или бронирование!');
            return response()->json($response, 200);
        }
        //if exists booking id check bike's serial num
        if (array_key_exists('booking_id', $data)) {
            $booking = $client->booking()->where('done', 0)->find($data['booking_id']);
            if (!$booking || $booking->bike_id != $bike->id) {
                $response = new ApiResponse(false, null, 'Бронирование этого велосипеда не найдено!');
                return response()->json($response, 200);
            }
            //set free bike status, done booking and calc it if all is right
            $booking->done = 1;
            $booking->save();
            CalcBookings::calcBooking($booking->id);
            $client->balance -= $booking->price;
            $client->save();
            $bike->status_id = 1;
        }
        if ($bike->status_id != 1) {
            $response = new ApiResponse(false, null, 'Велосипед недоступен!');
            return response()->json($response, 200);
        }
        //all is right, creating trip
        $bike->status_id = 3;
        $bike->save();
        $trip = $client->trips()->create([
            'bike_id'=>$bike->id,
            'tarif_type_id'=>(int)$data['tarif_type_id'],
            'status_id'=>3
        ]);
        $trip->history()->create([
            'status_id'=>3,
            'latitude'=>$bike->latitude,
            'longitude'=>$bike->longitude
        ]);
        $tarif_type = TarifType::find($data['tarif_type_id']);
        $tarif_id = $tarif_type->tarifs()->min('id');
        $tarif = Tarif::find($tarif_id);
        $trip->price_per_minute = $tarif->price;
        if ($client->balance < $tarif->price) {
            $response = new ApiResponse(false, null, 'Необходимо пополнить баланс!');
            return response()->json($response, 200);
        }
        if (!$bike->lock) {
            $response = new ApiResponse(false, null, 'Попробуйте другой велосипед!');
            return response()->json($response, 200);
        }
        $bike->lock->command()->create(['command'=>'UNLOCK#']);
        $bike->lock->command()->create(['command'=>'GPSON#']);
        $response = new ApiResponse(true, $trip);
        return response()->json($response, 200);
    }

    public function doneRent(Request $request)
    {
        $validation = [
            'trip_id'=>'required|exists:trips,id'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $data = $request->all();
        $client = Auth::user();
        //check trip
        $trip = $client->trips()->where('done', 0)->find($data['trip_id']);
        if (!$trip) {
            $response = new ApiResponse(false, null, 'Поездка не найдена!');
            return response()->json($response, 200);
        }
        //check for near parking & locked lock
        /*$parkings = Parking::all();
        $near_parking = false;
        foreach ($parkings as $parking){
            if ($this->getDistance($trip->bike->latitude, $trip->bike->longitude, $parking->latitude, $parking->longitude) <= 0.05) {
                $trip->bike->parking_id = $parking->id;
                $trip->bike->save();
                $near_parking = true;
            }
        }
        if (!$near_parking) {
            $trip->bike->lock->command()->create(['command'=>'UNLOCK#']);
            $response = new ApiResponse(false, null, 'Вы находитесь далеко от парковки!');
            return response()->json($response, 200);
        }*/
        /*if (!$trip->bike->lock) {
            $response = new ApiResponse(false, null, 'Попробуйте позже!');
            return response()->json($response, 200);
        }
        if (!$trip->bike->lock->locked) {
            $response = new ApiResponse(false, null, 'Необходимо закрыть замок!');
            return response()->json($response, 200);
        }*/

        //all is right, done rent
        $bike = $trip->bike;
        $trip->done = 1;
        $trip->save();
        $trip->history()->create([
            'status_id'=>1,
            'latitude'=>$bike->latitude,
            'longitude'=>$bike->longitude
        ]);
        $bike->status_id = 1;
        $bike->save();
        $client->balance -= $trip->price;
        if ($client->balance < 0) {
            if ($client->pledge) {
                $client->balance += $client->pledge->amount;
                $client->pledge->delete();
            }
        }
        $client->save();
        $coordinates_history = $trip->coordinates_history;
        $trip->coordinates_history = $coordinates_history;
        $minutes = 0;
        $last_time = null;
        foreach ($trip->history as $history) {
            if ($last_time) {
                $ride_time = strtotime($history->created_at) - strtotime($last_time);
                $minutes += $ride_time/60;
                $last_time = null;
            }
            if ($history->status_id == 3) {
                $last_time = $history->created_at;
            }
        }
        $distance = 0;
        foreach ($coordinates_history as $history) {
            if (isset($last_latitude) && isset($last_longitude)) {
                $current_distance = $this->getDistance($last_latitude, $last_longitude, $history->latitude, $history->longitude);
                $distance += $current_distance;
                $last_latitude = $history->latitude;
                $last_longitude = $history->longitude;
            }else{
                $last_latitude = $history->latitude;
                $last_longitude = $history->longitude;
            }
        }
        $trip->distance = round($distance, 2);
        $speed = $distance/($minutes/60);
        $trip->speed = round($speed, 2);
        $calories = ceil($minutes * 5.43);
        $trip->calories = $calories;
        $time = ceil((strtotime($trip->updated_at) - strtotime($trip->created_at))/60);
        $trip->time = $time;
        $bike->lock->command()->create(['command'=>'GPSOFF#']);
        $response = new ApiResponse(true, $trip, 'Поездка завершена!');
        return response()->json($response, 200);
    }

    public function checkTrip()
    {
        $client = Auth::user();
        $trip = $client->trips()->where('done', 0)->first();
        if ($trip) {
            $bike = $trip->bike;
            $trip->bike = $bike;
        }
        $response = new ApiResponse(true, $trip);
        return response()->json($response, 200);
    }

    private  function getDistance( $latitude1, $longitude1, $latitude2, $longitude2 ) {
        $earth_radius = 6371;

        $dLat = deg2rad( $latitude2 - $latitude1 );
        $dLon = deg2rad( $longitude2 - $longitude1 );

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }

}
