<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\Jobs\PromocodeAttempts;
use App\Promoaction;
use App\Promocode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PromocodeController extends Controller
{
    public function setPromocode(Request $request)
    {
        $validation = [
            'promocode'=>'required'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $client = Auth::user();
        if ($client->promocodes_attempt == 0) {
            $response = new ApiResponse(false, null, 'Закончились попытки ввода промокода, попробуйте через час!');
            return response()->json($response, 200);
        }
        $promocode = Promocode::where('code', $request->input('promocode'))->where('client_id', null)->first();
        $promoaction = Promoaction::where('code', $request->input('promocode'))->first();
        if (!$promocode && !$promoaction) {
            $client->promocodes_attempt -= 1;
            $client->save();
            if ($client->promocodes_attempt == 0) {
                $promocode_client = Client::find($client->id);
                PromocodeAttempts::dispatch($promocode_client)->delay(now()->addMinute(60));
            }
            $response = new ApiResponse(false, null, 'Промокод не найден!');
            return response()->json($response, 200);
        }
        if ($promocode) {
            $client->balance += $promocode->amount;
            $client->save();
            $promocode->client_id = $client->id;
            $promocode->save();
            $response = new ApiResponse(true, $promocode);
        }
        if ($promoaction) {
            $client->balance += $promoaction->amount;
            $client->save();
            DB::table('client_promoaction')->insert([
                'client_id'=>$client->id,
                'promoaction_id'=>$promoaction->id
            ]);
            $response = new ApiResponse(true, $promoaction);
        }
        return response()->json($response, 200);
    }

    public function getPromocodes()
    {
        $client = Auth::user();
        $promocodes = $client->promocodes;
        $response = new ApiResponse(true, $promocodes);
        return response()->json($response, 200);
    }

    public function getPromoactions()
    {
        $client = Auth::user();
        $promoactions = $client->promoactions;
        $response = new ApiResponse(true, $promoactions);
        return response()->json($response, 200);
    }
}
