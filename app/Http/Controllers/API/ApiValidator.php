<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ApiValidator extends Controller
{
    public static function validateRequest(array $rules, Request $request)
    {
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response = new ApiResponse(false, null, 'Ошибки валидации!', $validator->errors());
            return $response;
        }
        return 0;
    }
}
