<?php

namespace App\Http\Controllers\API;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function updateProfile(Request $request)
    {
        $validation = [
            'username'=>'nullable|string|unique:clients',
            'email'=>'nullable|email||unique:clients',
            'growth'=>'nullable|numeric',
            'weight'=>'nullable|numeric',
            'photo'=>'file',
            'latitude'=>'numeric',
            'longitude'=>'numeric'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $client = Auth::user();
        Client::where('id', $client->id)->update($request->only('username','email','growth','weight','latitude','longitude'));
        if ($request->file('photo')) {
            if ($client->photo) {
                $path = public_path('/storage/avatars/' . $client->photo);
                if (file_exists($path)) {
                    unlink($path);
                }
            }
            $guessExtension = $request->file('photo')->guessExtension();
            $photo_id = $client->id;
            $request->file('photo')->storeAs('public/avatars', 'user'.$photo_id.'avatar'.'.'.$guessExtension );
            $client->photo = 'user'.$photo_id.'avatar'.'.'.$guessExtension;
            $client->save();
        }
        $response = new ApiResponse(true, $client);
        return response()->json($response, 200);

    }

    public function getCurrentUser()
    {
        $client = Auth::user();
        $response = new ApiResponse(true, $client);
        return response()->json($response, 200);
    }
}
