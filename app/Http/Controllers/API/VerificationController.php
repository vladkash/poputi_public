<?php

namespace App\Http\Controllers\API;

use App\Verification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;

class VerificationController extends Controller
{
    public function verify(Request $request)
    {
        $validation = [
            'verification_id'=>'required'
        ];
        $check = ApiValidator::validateRequest($validation, $request);
        if ($check) {
            return response()->json($check, 200);
        }
        $client = Auth::user();
        $exist_verification = $client->verification;
        if ($exist_verification) {
            $response = new ApiResponse(false, null, 'Запись верификации уже существует!');
            return response()->json($response, 200);
        }
        $verification = $client->verification->create([
            'verification_id'=>$request->input('verification_id')
        ]);
        $response = new ApiResponse(true, $verification);
        return response()->json($response, 200);
    }

    public function checkVerification()
    {
        $client = Auth::user();
        $verification = $client->verification;
        if (!$verification) {
            $response = new ApiResponse(false, null, 'Не создана запись верификации!');
            return response()->json($response, 200);
        }
        if ($verification->passport_number != null) {
            $response = new ApiResponse(false, null, 'Проверка верификации уже произведена!');
            return response()->json($response, 200);
        }
        $headers = [
            'Content-Type' => 'multipart/form-data'
        ];
        $client = new Client(['headers' => $headers]);
        $result = $client->post('https://api.checku.co/', [
            'form_params' => [
                'token'=>'7905e2a6fbd71346ef9df4b5675d89cf',
                'id' => $verification->verification_id
            ]
        ]);
        $data = json_decode($result,true);
        //check for different states
        if ($data['state'] == 'PROCESSING') {
            $response = new ApiResponse(false, null, 'Верификация в процессе выполнения!');
            return response()->json($response, 200);
        }
        if ($data['state'] == 'ERROR') {
            $client->verification->delete();
            $response = new ApiResponse(false, null, 'Ошибка верификации! На изображении нет документа!');
            return response()->json($response, 200);
        }
        if ($data['state'] != 'COMPLETE') {
            $response = new ApiResponse(false, null, 'Неизвестный статус верификации!');
            return response()->json($response, 200);
        }
        $verification_data = $data['data'];
        if ($verification_data['number']) {
            $verification->passport_number = $verification_data['number'];
        }
        if ($verification_data['surname']) {
            $client->surname = $verification_data['surname'];
        }
        if ($verification_data['name']) {
            $client->name = $verification_data['name'];
        }
        if ($verification_data['patronymic']) {
            $verification->patronymic = $verification_data['patronymic'];
        }
        if ($verification_data['sex']) {
            if ($verification_data['sex'] == 'МУЖ') {
                $client->sex_id = 1;
            }else{
                $client->sex_id = 2;
            }
        }
        if ($verification_data['birthday']) {
            $client->birthdate = date('Y-m-d', strtotime($verification_data['birthday']));
        }
        if ($verification_data['birthplace']) {
            $verification->birthplace = $verification_data['birthplace'];
        }
        if ($verification_data['deliverydate']) {
            $verification->deliverydate = $verification_data['deliverydate'];
        }
        if ($verification_data['deliveryplace']) {
            $verification->deliveryplace = $verification_data['deliveryplace'];
        }
        if ($verification_data['departmentcode']) {
            $verification->departmentcode = $verification_data['departmentcode'];
        }
        $client->verificated = 1;
        $client->save();
        $verification->save();
        $response = new ApiResponse(true, $verification);
        return response()->json($response, 200);
    }
}
