<?php

namespace App\Http\Controllers\Lock;


use App\Bike;
use App\Jobs\GetCoordinates;
use App\Lock;

class ConcoxProtocol
{
    private $message;
    private $start_bit;
    private $protocol_number;
    private $length;
    private $resource_id;
    private $info_serial_num;
    public $response = '';

    public function __construct($protocol_message, $resource_id)
    {
        $this->message = $protocol_message;
        $this->resource_id = $resource_id;
        $this->info_serial_num = substr($protocol_message, -12, 4);
        $this->start_bit = substr($protocol_message, 0, 4);
        if ($this->start_bit == '7878') {
            $this->length = substr($protocol_message, 4, 2);
            $this->protocol_number = substr($protocol_message, 6, 2);
            switch ($this->protocol_number) {
                case '01':
                    $this->makeLoginResponse();
                    break;
                case '23':
                    $this->makeHeartbeatResponse();
                    break;
            }
        } elseif ($this->start_bit == '7979') {
            $this->length = substr($protocol_message, 4, 4);
            $this->protocol_number = substr($protocol_message, 8, 2);
            switch ($this->protocol_number) {
                case '32':
                    $this->makeLocationPacket();
                    break;
                case '33':
                    $this->makeAlarmPacket();
                    break;
                case '98':
                    $this->makeTransmissionPacket();
                    break;
                case '80':
                    $this->makeTransmissionPacket();
                    break;
            }
        }
    }

    private function makeLoginResponse()
    {
        $imei = substr($this->message, 8, 16);
        $lock = Lock::where('resource_id', $this->resource_id)->first();
        if ($lock) {
            $lock->imei = $imei;
            $lock->info_serial_num = $this->info_serial_num;
            $lock->save();
            $bike = Bike::where('imei', $imei)->first();
            if ($bike) {
                $bike->lock_id = $lock->id;
                $bike->save();
            }
            GetCoordinates::dispatch($lock)->delay(now()->addSeconds(10));
            $this->makeDefaultResponse('0c', date('ymdHis') . '00');
        }

    }

    private function makeHeartbeatResponse()
    {
        $terminal_info_content = substr($this->message, 8, 2);
        $voltage_level = hexdec(substr($this->message, 10, 4))/100;
        $voltage = $this->getVoltage($voltage_level);
        $dec = hexdec($terminal_info_content);
        $locked = $dec % 2;
        Lock::where('resource_id', $this->resource_id)->update([
            'locked'=>$locked,
            'info_serial_num'=>$this->info_serial_num,
            'voltage'=>$voltage
        ]);
        $this->makeDefaultResponse('05', '');
    }

    private function makeLocationPacket()
    {
        $gps_info = substr($this->message, 22, 2);
        if ($gps_info != '00') {
            $latitude_hex = substr($this->message, 26, 8);
            $latitude = hexdec($latitude_hex)/1800000;
            $longitude_hex = substr($this->message, 34, 8);
            $longitude = hexdec($longitude_hex)/1800000;
            $lock = Lock::where('resource_id', $this->resource_id)->first();
            if ($lock->bike) {
                $lock->bike()->update([
                    'longitude' => $longitude,
                    'latitude'=>$latitude
                ]);
            }
            $trip = $lock->bike->trips()->where('done', 0)->first();
            if ($trip) {
                $trip->coordinates_history()->create([
                    'longitude' => $longitude,
                    'latitude'=>$latitude
                ]);
            }
            $lock->info_serial_num = $this->info_serial_num;
            $lock->save();
            $this->makeDefaultResponse('0005', '');
        }else{
            $this->makeDefaultResponse('0005', '');
        }
    }
    private function makeTransmissionPacket()
    {
        $lock = Lock::where('resource_id', $this->resource_id)->first();
        $lock->info_serial_num = $this->info_serial_num;
        $lock->save();
        $bt_mac = $this->getBtMac($this->message);
        $bike = Bike::where('bt_mac',$bt_mac)->first();
        if ($bike) {
            $bike->imei = $lock->imei;
            $bike->lock_id = $lock->id;
            $bike->save();
        }
        $this->makeDefaultResponse('0006', '00');
    }

    private function getBtMac($str)
    {
        $module_num = '00';
        $pos = 12;
        while ($module_num != '04') {
            $module_len = hexdec(substr($str, $pos, 4));
            $pos += $module_len * 2+4;
            $module_num = substr($str, $pos, 2);
            $pos += 2;
        }
        $bt_len = hexdec(substr($str, $pos, 4));
        $pos += 4;
        return substr($str, $pos, $bt_len*2);

    }

    private function getVoltage($voltage_level)
    {
        if ($voltage_level == 4.16) {
            return '100%';
        }
        if ($voltage_level < 4.16 && $voltage_level >= 4.11) {
            return '95%';
        }
        if ($voltage_level < 4.11 && $voltage_level >= 4.08) {
            return '90%';
        }
        if ($voltage_level < 4.08 && $voltage_level >= 4.04) {
            return '90%';
        }
        if ($voltage_level < 4.04 && $voltage_level >= 4.01) {
            return '80%';
        }
        if ($voltage_level < 4.01 && $voltage_level >= 3.98) {
            return '75%';
        }
        if ($voltage_level < 3.98 && $voltage_level >= 3.95) {
            return '70%';
        }
        if ($voltage_level < 3.95 && $voltage_level >= 3.92) {
            return '65%';
        }
        if ($voltage_level < 3.92 && $voltage_level >= 3.88) {
            return '60%';
        }
        if ($voltage_level < 3.88 && $voltage_level >= 3.85) {
            return '55%';
        }
        if ($voltage_level < 3.85 && $voltage_level >= 3.82) {
            return '50%';
        }
        if ($voltage_level < 3.82 && $voltage_level >= 3.79) {
            return '45%';
        }
        if ($voltage_level < 3.79 && $voltage_level >= 3.76) {
            return '40%';
        }
        if ($voltage_level < 3.76 && $voltage_level >= 3.72) {
            return '35%';
        }
        if ($voltage_level < 3.72 && $voltage_level >= 3.69) {
            return '30%';
        }
        if ($voltage_level < 3.69 && $voltage_level >= 3.66) {
            return '25%';
        }
        if ($voltage_level < 3.66 && $voltage_level >= 3.63) {
            return '20%';
        }
        if ($voltage_level < 3.63 && $voltage_level >= 3.60) {
            return '15%';
        }
        if ($voltage_level < 3.60 && $voltage_level >= 3.56) {
            return '10%';
        }
        if ($voltage_level < 3.56 && $voltage_level >= 3.54) {
            return '5%';
        }
        return '0%';
    }
    private function makeAlarmPacket()
    {
        $lock = Lock::where('resource_id', $this->resource_id)->first();
        $lock->info_serial_num = $this->info_serial_num;

        $report = substr($this->message, 30, 2);
        switch ($report) {
            case 'a0':
                $lock->locked = 1;
                break;
            case 'a1':
                $lock->locked = 0;
        }
        $lock->save();
        $this->makeDefaultResponse('0005', '');
    }
    private function makeDefaultResponse($length, $content)
    {
        $this->response .= $length;
        $this->response .= $this->protocol_number;
        $this->response .= $content;
        $this->response .= $this->info_serial_num;
        $this->response .= CRC16::ComputeCrc($this->response);
        $this->response = $this->start_bit . $this->response . '0d0a';
    }
}