<?php

namespace App\Http\Controllers\Lock;


class CRC16 {


    public static function ComputeCrc($data) {
        $data = str_split($data, 2);
        $crcParams = new CrcParams();

        if ($crcParams->RefIn) {
            $crc = $crcParams->InvertedInit;
        } else {
            $crc = $crcParams->Init;
        }
        if ($crcParams->RefOut) {
            foreach ($data as $d) {
                $crc = $crcParams->Array[(hexdec($d) ^ $crc) & 0xFF] ^ ($crc >> 8 & 0xFF);
            }
        } else {
            foreach ($data as $d) {
                $crc = $crcParams->Array[(($crc >> 8) ^ hexdec($d)) & 0xFF] ^ ($crc << 8);
            }
        }

        $crc = $crc ^ $crcParams->XorOut;

        $result = $crc & 0xFFFF;
        $hexFormat="%'.04x";
        return sprintf($hexFormat, $result);
    }
}