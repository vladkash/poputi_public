<?php

namespace App\Http\Controllers\Lock;

use App\Command;
use App\Lock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LockController extends Controller
{
    public $deleteCommand = false;
    public $command;

    public function createLock(Request $request)
    {
        $exist = Lock::where('resource_id', $request->input('resource_id'))->first();
        if (!$exist) {
            Lock::create([
                'resource_id' => $request->input('resource_id')
            ]);
            return 1;
        }
        return 0;
    }
    public function makeResponse(Request $request)
    {
        $data = $request->all();
        $protocol = new ConcoxProtocol($data['message'], $data['resource_id']);
        return $protocol->response;
    }

    public function checkCommand()
    {
        $command = Command::first();
        if ($command) {
            $this->deleteCommand = true;
            $this->command = $command;
            $protocol_command = new ServerCommand($command->command, $command->lock);
            return $protocol_command->response . ' ' . $command->lock->resource_id;
        }else{
            return 0;
        }
    }

    public function deleteLock(Request $request)
    {
        Lock::where('resource_id', $request->input('resource_id'))->delete();
    }

    public function __destruct()
    {
        if ($this->deleteCommand) {
            $this->command->delete();
        }
    }
}
