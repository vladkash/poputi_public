<?php

namespace App\Http\Controllers\Lock;


use App\Lock;

class ServerCommand
{
    public $response = '';
    private $protocol_number;
    private $info_serial_num;
    private $start_bit;
    private $command = '';

    public function __construct($command, Lock $lock)
    {
        $this->start_bit = '7878';
        $this->protocol_number = '80';
        $this->info_serial_num = dechex(hexdec($lock->info_serial_num) + 1);
        $count_null = 4 - strlen($this->info_serial_num);
        for ($i = 0; $i < $count_null; $i++) {
            $this->info_serial_num = '0' . $this->info_serial_num;
        }
        $lock->info_serial_num = $this->info_serial_num;
        $lock->save();
        $command_chars = str_split($command);
        foreach ($command_chars as $command_char) {
            $this->command .= dechex(ord($command_char));
        }
        $dec_len = 4 + strlen($this->command) / 2;
        $hex_len_command = dechex($dec_len);
        if ($dec_len < 16) {
            $hex_len_command = '0'.$hex_len_command;
        }
        $content = '';
        $content .= $hex_len_command;
        $content .= '00000000';
        $content .= $this->command;
        //$content .= '0002';
        $length = dechex(strlen($content)/2 + 5);
        if (strlen($length) == 1) {
            $length = '0' . $length;
        }
        $this->makeDefaultResponse($length, $content);
    }

    private function makeDefaultResponse($length, $content)
    {
        $this->response .= $length;
        $this->response .= $this->protocol_number;
        $this->response .= $content;
        $this->response .= $this->info_serial_num;
        $this->response .= CRC16::ComputeCrc($this->response);
        $this->response = $this->start_bit . $this->response . '0d0a';
    }
}