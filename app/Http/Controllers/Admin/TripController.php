<?php

namespace App\Http\Controllers\Admin;

use App\Trip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class TripController extends Controller
{
    public function getTrips()
    {
        $search = $this->searchTrip();
        if ($search !== 0) {
            return $search;
        }
        $route_name = Route::currentRouteName();
        $title = 'Поездки';
        $trips = Trip::orderBy('id', 'desc')->paginate(15);
        return view('admin.trips.list',compact('route_name','title','trips'));
    }

    public function getTripInfo($trip_id)
    {
        $route_name = Route::currentRouteName();
        $title = 'Информация о поездке';
        $trip = Trip::find($trip_id);
        return view('admin.trips.info', compact('route_name', 'title', 'trip'));

    }
    private function searchTrip()
    {
        if (isset($_GET['search_trips']))
        {
            $search = trim(urldecode($_GET['search_trips']));
            if (empty($search))
            {
                $trips = Trip::orderBy('id', 'desc')->paginate(15);
            }
            else
            {
                $trips = Trip::orWhere('id', 'LIKE', "%{$search}%")
                    ->orWhere('client_id', 'LIKE', "%{$search}%")
                    ->orWhere('bike_id', 'LIKE', "%{$search}%")
                    ->orWhere('tarif_type_id', 'LIKE', "%{$search}%")
                    ->orWhere('created_at', 'LIKE', "%{$search}%")
                    ->get();
            }
            return view('admin.trips.table', compact('trips'));
        }
        return 0;
    }
}
