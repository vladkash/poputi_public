<?php

namespace App\Http\Controllers\Admin;

use App\BikeModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class BikeModelController extends Controller
{
    public function getModels()
    {
        $title = "Модели велосипедов";
        $route_name = Route::currentRouteName();
        $models = BikeModel::all();
        return view('admin.models_types.models', compact("title", "route_name", "models"));
    }

    public function editModel($edit_id)
    {
        $title = "Редактировать модель";
        $route_name = Route::currentRouteName();
        $edit = BikeModel::find($edit_id);
        $delete = 'models';
        return view('admin.models_types.form', compact('title', 'route_name', 'edit', 'delete'));
    }

    public function makeModelChanges(Request $request, $edit_id)
    {
        $request->validate([
            'edit_name'=>'required'
        ]);
        $model = BikeModel::find($edit_id);
        if ($model->name != $request->input('edit_name')) {
            $request->validate([
                'edit_name'=>'unique:bike_models,name'
            ]);
        }
        $model->update(['name'=>$request->input('edit_name')]);
        return redirect('/admin/models');
    }

    public function getAddModel()
    {
        $title = "Добавить модель";
        $route_name = Route::currentRouteName();
        return view('admin.models_types.form', compact('title', 'route_name'));
    }

    public function addModel (Request $request)
    {
        $request->validate([
            'edit_name'=>'required|unique:bike_models,name'
        ]);
        BikeModel::create([
            'name'=>$request->input('edit_name')
        ]);
        return redirect('/admin/models');
    }

    public function deleteModel($delete_id)
    {
        BikeModel::find($delete_id)->delete();
        return redirect('/admin/models');
    }
}
