<?php

namespace App\Http\Controllers\Admin;

use App\Tarif;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class TarifController extends Controller
{
    public function getTarifs()
    {
        $route_name = Route::currentRouteName();
        $title = 'Настройки';
        return view('admin.tarifs.list', compact('route_name', 'title'));
    }

    public function changeTarif($tarif_id, Request $request)
    {
        $price = explode(' ', $request->input('price'))[0];
        if(is_numeric($price)){
            $tarif = Tarif::find($tarif_id);
            $tarif->price = $price;
            $tarif->save();
        }
        return redirect()->back();
    }
}
