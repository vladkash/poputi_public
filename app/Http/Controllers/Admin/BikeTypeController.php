<?php

namespace App\Http\Controllers\Admin;

use App\BikeType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class BikeTypeController extends Controller
{
    public function getTypes()
    {
        $title = "Типы велосипедов";
        $route_name = Route::currentRouteName();
        $types = BikeType::all();
        return view('admin.models_types.types', compact("title", "route_name", "types"));
    }

    public function editType($edit_id)
    {
        $title = "Редактировать тип";
        $route_name = Route::currentRouteName();
        $edit = BikeType::find($edit_id);
        $delete = 'types';
        return view('admin.models_types.form', compact('title', 'route_name', 'edit', 'delete'));
    }

    public function makeTypeChanges(Request $request, $edit_id)
    {
        $request->validate([
            'edit_name'=>'required'
        ]);
        $type = BikeType::find($edit_id);
        if ($type->name != $request->input('edit_name')) {
            $request->validate([
                'edit_name'=>'unique:bike_types,name'
            ]);
        }
        $type->update(['name'=>$request->input('edit_name')]);
        return redirect('/admin/types');
    }

    public function getAddType()
    {
        $title = "Добавить тип";
        $route_name = Route::currentRouteName();
        return view('admin.models_types.form', compact('title', 'route_name'));
    }

    public function addType (Request $request)
    {
        $request->validate([
            'edit_name'=>'required|unique:bike_types,name'
        ]);
        BikeType::create([
            'name'=>$request->input('edit_name')
        ]);
        return redirect('/admin/types');
    }

    public function deleteType($delete_id)
    {
        BikeType::find($delete_id)->delete();
        return redirect('/admin/models');
    }
}
