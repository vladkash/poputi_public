<?php

namespace App\Http\Controllers\Admin;

use App\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class TicketController extends Controller
{
    public function getTickets()
    {
        $search = $this->searchTicket();
        if ($search !== 0) {
            return $search;
        }
        $title = 'Тикеты';
        $route_name = Route::currentRouteName();
        $tickets = Ticket::orderBy('id','desc')->paginate(25);
        return view('admin.tickets.list', compact('title', 'route_name', 'tickets'));
    }

    public function getTicket($ticket_id)
    {
        $title = 'Тикет '.$ticket_id;
        $route_name = Route::currentRouteName();
        $ticket = Ticket::find($ticket_id);
        $messages = $ticket->messages()->orderBy('id','desc')->paginate(20);
        return view('admin.tickets.view', compact('title', 'route_name', 'ticket','messages'));
    }

    public function sendMessage($ticket_id, Request $request)
    {
        $ticket = Ticket::find($ticket_id);
        $ticket->messages()->create([
            'is_admin'=>1,
            'text'=>$request->input('text')
        ]);
        return redirect()->back();
    }

    public function changeStatus($ticket_id, $status_id)
    {
        Ticket::find($ticket_id)->update(['status_id'=>$status_id]);
        return redirect()->back();
    }

    private function searchTicket()
    {
        if (isset($_GET['search_ticket'])) {
            $search = urldecode($_GET['search_ticket']);
            if (empty($search)) {
                $tickets = Ticket::all();
            } else {
                $tickets = Ticket::orWhere('subject', 'LIKE', "%{$search}%")
                    ->orWhere('text', 'LIKE', "%{$search}%")
                    ->orWhere('client_id', 'LIKE', "%{$search}%")->get();
            }
            return view('admin.tickets.table',compact('tickets'));
        }
        return 0;
    }
}
