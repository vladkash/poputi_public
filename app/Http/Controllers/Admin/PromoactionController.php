<?php

namespace App\Http\Controllers\Admin;

use App\Promoaction;
use App\Promocode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class PromoactionController extends Controller
{
    public function getPromoactions()
    {
        $title = "Промоакции";
        $route_name = Route::currentRouteName();
        $promoactions = Promoaction::orderBy('id', 'desc')->paginate(15);
        return view('admin.promo.list', compact("title", "route_name", "promoactions"));
    }

    public function getAddPromoaction()
    {
        $title = "Добавить промоакцию";
        $route_name = Route::currentRouteName();
        return view('admin.promo.form', compact("title","route_name"));
    }

    public function addPromoaction(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'amount' => 'required|numeric',
            'code'=>'unique:promoactions|unique:promocodes'
        ]);
        $data = $request->all();
        if (!array_key_exists('code', $data) || empty($data['code'])) {
            $data['code'] = str_random(10);
            while (Promoaction::where('code', $data['code'])->first()) {
                $data['code'] = str_random(10);
            }
            while (Promocode::where('code', $data['code'])->first()) {
                $data['code'] = str_random(10);
            }
        }
        Promoaction::create($data);
        return redirect('/admin/promoactions');
    }

    public function editPromoaction($promoaction_id)
    {
        $title = "Редактировать промоакцию";
        $route_name = Route::currentRouteName();
        $promoaction = Promoaction::find($promoaction_id);
        return view('admin.promo.form', compact('title', 'route_name', 'promoaction'));
    }

    public function makePromoactionChanges($promoaction_id, Request $request)
    {
        $request->validate([
            'name'=>'required',
            'amount' => 'required|numeric',
        ]);
        $promoaction = Promoaction::find($promoaction_id);
        if ($promoaction->code != $request->input('code')) {
            $request->validate([
                'code'=>'unique:promoactions|unique:promocodes'
            ]);
        }
        $promoaction->update($request->all());
        return redirect('/admin/promoactions');
    }

    public function deletePromoaction($promoaction_id)
    {
        Promoaction::find($promoaction_id)->delete();
        return redirect('/admin/promoactions');
    }
}
