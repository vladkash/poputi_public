<?php

namespace App\Http\Controllers\Admin;

use App\Bike;
use App\Client;
use App\Parking;
use App\Payment;
use App\Pledge;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class PagesController extends Controller
{

    public function welcome($date = 'all')
    {
        $title = "Dashboard";
        $route_name = Route::currentRouteName();
        $count_bikes = Bike::count();
        $count_parkings = Parking::count();
        $count_users = User::count();
        $count_break_bikes = Bike::where('status_id', 5)->count();
        $count_working_bikes = Bike::where('status_id', '!=', 5)->where('status_id', '!=', 6)->count();

        switch ($date) {
            case 'day':
                $date = date('Y-m-d H:i:s',strtotime('-1 day'));
                $title_date = 'День';
                break;
            case 'week':
                $date = date('Y-m-d H:i:s',strtotime('-7 days'));
                $title_date = 'Неделю';
                break;
            case 'month':
                $date = date('Y-m-d H:i:s',strtotime('-1 month'));
                $title_date = 'Месяц';
                break;
            default:
                $date = '2018-08-01 00:00:00';
                $title_date = 'Все время';     
        }

        $date_new_users = Client::whereDate('created_at', '>=', $date)->count();
        $date_new_in_pledges = Pledge::whereDate('created_at', '>=', $date)->count();
        $date_new_out_pledges = Pledge::onlyTrashed()->whereDate('created_at', '>=', $date)->count();
        $date_new_payments = Payment::whereDate('created_at', '>=', $date)->where('type_id',1)->where('status_id',1)->count();
        return view('admin.dashboard.home',compact(
            "route_name",
            "count_bikes",
            "count_parkings",
            "count_users",
            "count_break_bikes",
            "count_working_bikes",
            "title",
            "date_new_users",
            "date_new_in_pledges",
            "date_new_out_pledges",
            "date_new_payments",
            "title_date"
            ));
    }

    public function index()
    {
        return redirect('/admin/dashboard');
    }
}
