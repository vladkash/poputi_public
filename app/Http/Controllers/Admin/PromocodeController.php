<?php

namespace App\Http\Controllers\Admin;

use App\Promoaction;
use App\Promocode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class PromocodeController extends Controller
{
    public function getCreatePromocode()
    {
        $title = "Добавить промокод";
        $route_name = Route::currentRouteName();
        return view('admin.promo.form', compact("title","route_name"));
    }

    public function createPromocode(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'amount' => 'required|numeric',
            'code' => 'unique:promoactions|unique:promocodes'
        ]);
        $data = $request->all();
        if (!array_key_exists('code', $data) || empty($data['code'])) {
            $data['code'] = str_random(10);
            while (Promoaction::where('code', $data['code'])->first()) {
                $data['code'] = str_random(10);
            }
            while (Promocode::where('code', $data['code'])->first()) {
                $data['code'] = str_random(10);
            }
        }
        $promocode = Promocode::create($data);
        return redirect('/admin/promoactions')->with(['message' => 'Создан промокод   ' . $promocode->code . '   на сумму ' . $promocode->amount . ' рублей']);
    }
}
