<?php

namespace App\Http\Controllers\Admin;

use App\Parking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class ParkingController extends Controller
{
    public function getParkings()
    {
        $search = $this->searchParkings();
        if ($search !== 0) {
            return $search;
        }
        $title = "Парковки";
        $route_name = Route::currentRouteName();
        $parkings = Parking::paginate(15);
        $parkings = $this->makeParkings($parkings);
        return view('admin.parkings.list', compact('title', 'route_name', 'parkings'));
    }

    public function editParking($parking_id)
    {
        $title = "Редактировать парковку";
        $route_name = Route::currentRouteName();
        $parking = Parking::find($parking_id);
        $bikes = $parking->bikes()->paginate(15);
        return view('admin.parkings.form', compact('title','parking','route_name','bikes'));
    }

    public function makeParkingChanges(Request $request, $parking_id)
    {
        $request->validate([
            'is_work'=>'required',
            'latitude'=>'required|numeric',
            'longitude'=>'required|numeric'
        ]);
        $parking = Parking::find($parking_id);
        if ($parking->name != $request->input('name')) {
            $request->validate([
                'name'=>'required|unique:parkings',
            ]);
        }
        $parking->update($request->only('name', 'is_work', 'latitude', 'longitude','longitude1','latitude1','longitude2','latitude2'));
        return redirect()->back();
    }

    public function getAddParking()
    {
        $title = "Добавить парковку";
        $route_name = Route::currentRouteName();
        return view('admin.parkings.form', compact('title','route_name'));
    }

    public function addParking(Request $request)
    {
        $request->validate([
            'name'=>'required|unique:parkings',
            'is_work'=>'required',
            'latitude'=>'required|numeric',
            'longitude'=>'required|numeric'
        ]);
        Parking::create($request->only('name', 'is_work', 'latitude', 'longitude','longitude1','latitude1','longitude2','latitude2'));
        return redirect('/admin/parkings');
    }

    public function deleteParking($parking_id)
    {
        Parking::find($parking_id)->delete();
        return redirect('/admin/parkings');
    }

    private function searchParkings()
    {
        if (isset($_GET['search_park']))
        {
            $search = trim(urldecode($_GET['search_park']));
            $res = '';

            if (empty($search))
            {
                $parkings = Parking::paginate(15);
            }
            else
            {
                $parkings = Parking::orWhere('id', 'LIKE', "%{$search}%")
                    ->orWhere('name', 'LIKE', "%{$search}%")
                    ->orWhere('latitude', 'LIKE', "%{$search}%")
                    ->orWhere('longitude', 'LIKE', "%{$search}%")
                    ->get();
            }

            if ((mb_stripos($search, 'Не') === 0))
            {
                $parkings = Parking::where('is_work', 0)->get();
            }

            if ((mb_stripos($search, 'Ра') === 0))
            {
                $parkings = Parking::where('is_work', 1)->get();
            }
            $parkings = $this->makeParkings($parkings);
            return view('admin.parkings.table', compact('parkings'));
        }
        return 0;
    }

    private function makeParkings($parkings)
    {
        foreach ($parkings as $parking) {
            if ($parking->is_work) {
                $parking->class = 'tr_green';
                $parking->status = 'Работает';
            }else{
                $parking->class = 'tr_pink';
                $parking->status = 'Не работает';
            }

        }
        return $parkings;
    }
}
