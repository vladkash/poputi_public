<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class ClientController extends Controller
{
    public function getClients()
    {
        $search = $this->searchClients();
        if ($search !== 0) {
            return $search;
        }
        $route_name = Route::currentRouteName();
        $title = 'Пользователи';
        $clients = Client::orderBy('id','desc')->paginate(15);
        return view('admin.clients.list',compact('route_name','title','clients'));

    }

    public function getAddClient()
    {
        $route_name = Route::currentRouteName();
        $title = 'Добавить пользователя';
        return view('admin.clients.form',compact('route_name','title'));
    }

    public function addClient(Request $request)
    {
        $request->validate([
            'surname'=>'required',
            'name'=>'required',
            'sex_id'=>'required',
            'phone'=>'required|unique:clients',
            'verificated'=>'required',
            'is_locked'=>'required',
            'growth'=>'required|numeric',
            'weight'=>'required|numeric',
            'email'=>'required|email|unique:clients',
            'birthdate'=>'required|date'
        ]);
        Client::create($request->all());
        return redirect('/admin/clients');
    }

    public function editClient($client_id)
    {
        $route_name = Route::currentRouteName();
        $title = 'Редактировать пользователя';
        $client = Client::find($client_id);
        return view('admin.clients.form',compact('route_name','title', 'client'));
    }

    public function makeClientChanges(Request $request, $client_id)
    {
        $request->validate([
            'surname'=>'required',
            'name'=>'required',
            'sex_id'=>'required',
            'verificated'=>'required',
            'is_locked'=>'required',
            'growth'=>'required|numeric',
            'weight'=>'required|numeric',
            'birthdate'=>'required|date'
        ]);
        $client = Client::find($client_id);
        if ($client->phone != $request->input('phone')) {
            $request->validate([
                'phone'=>'required|unique:clients'
            ]);
        }
        if ($client->email != $request->input('email')) {
            $request->validate([
                'email'=>'required|email|unique:clients',
            ]);
        }
        $client->update($request->except('birthdate'));
        $birthdate = date('Y-m-d', strtotime($request->input('birthdate')));
        $client->update(compact('birthdate'));
        return redirect()->back();
    }

    public function getClientInfo($client_id)
    {
        $route_name = Route::currentRouteName();
        $title = 'Информация о клиенте';
        $client = Client::find($client_id);
        $payments = $client->payments;
        $client->payments_amount = 0;
        foreach ($payments as $payment) {
            $client->payments_amount += $payment->amount;
        }
        $trips = $client->trips()->orderBy('id', "desc")->paginate(15);
        $payments = $client->payments()->orderBy('id', "desc")->paginate(15);
        return view('admin.clients.info', compact('route_name','title','client','trips','payments'));
    }

    public function deleteClient($client_id)
    {
        $client = Client::find($client_id);
        $client->delete();
        return redirect('/admin/clients');
    }
    private function searchClients()
    {
        if (isset($_GET['search_client']))
        {
            $search = trim(urldecode($_GET['search_client']));
            if (empty($search)) {
                $clients = Client::orderBy('id','desc')->paginate(15);
            }
            else {
                $clients = Client::orWhere('id', 'LIKE', "%{$search}%")
                    ->orWhere('phone', 'LIKE', "%{$search}%")
                    ->orWhere('surname', 'LIKE', "%{$search}%")
                    ->orWhere('name', 'LIKE', "%{$search}%")
                    ->orWhere('email', 'LIKE', "%{$search}%")
                    ->get();
            }
            if ((stripos($search, 'акт'))) {
                $clients = Client::where('is_locked', 0)->get();
            }

            if ((stripos($search, 'забл'))) {
                $clients = Client::where('is_locked', 1)->get();
            }
            $clients = $this->makeClients($clients);
            return view('admin.clients.table', compact('clients'));
        }
        return 0;
    }

    private function makeClients($clients)
    {
        foreach ($clients as $client) {
            if (!empty($client->birthdate)) {
                $client->birthdate = date('d.m.Y', strtotime($client->birthdate));
            }
        }
        return $clients;
    }
}

