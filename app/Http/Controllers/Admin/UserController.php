<?php

namespace App\Http\Controllers\Admin;

use App\Mail\SendPassword;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

class UserController extends Controller
{
    public function getUsers()
    {
        $title = 'Менеджеры';
        $route_name = Route::currentRouteName();
        $users = Role::where('name', 'manager')->first()->users()->orderBy('id', 'desc')->paginate(15);
        return view('admin.users.list', compact('title', 'route_name', 'users'));
    }

    public function getAddUser()
    {
        $title = 'Добавить менеджера';
        $route_name = Route::currentRouteName();
        return view('admin.users.form', compact('title', 'route_name'));
    }

    public function addUser(Request $request)
    {
        $request->validate([
            'name'=>'unique:users',
            'email'=>'email|unique:users'
        ]);
        $data = $request->all();
        if (!array_key_exists('password', $data) || empty($data['password'])) {
            $data['password'] = str_random(16);
        }
        Mail::to($request->input('email'))->send(new SendPassword($request->input('email'), $data['password']));
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        $user->role_id = 2;
        $user->save();

        return redirect('/admin/users');
    }

    public function editUser($user_id)
    {
        $title = 'Добавить менеджера';
        $route_name = Route::currentRouteName();
        $user = User::find($user_id);
        return view('admin.users.form', compact('title', 'route_name','user'));
    }

    public function makeUserChanges($user_id, Request $request)
    {
        $request->validate([
            'email'=>'email',
            'password'=>'required'
        ]);
        $user = User::find($user_id);
        if ($user->name != $request->input('name')) {
            $request->validate([
                'name'=>'unique:users'
            ]);
        }
        if ($user->email != $request->input('email')) {
            $request->validate([
                'email'=>'unique:users'
            ]);
        }
        $user->update($request->all());
        return redirect()->back();
    }

    public function deleteUser($user_id)
    {
        User::find($user_id)->delete();
        return redirect('/admin/users');
    }

    public function getUserInfo($user_id)
    {
        $title = 'Информация о менеджере';
        $route_name = Route::currentRouteName();
        $user = User::find($user_id);
        return view('admin.users.info', compact('title', 'route_name','user'));
    }
}
