<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class PaymentController extends Controller
{
    public function getFinances()
    {
        $route_name = Route::currentRouteName();
        $title = 'Финансы';
        $payments = Payment::orderBy('id','desc')->paginate(15);
        return view('admin.payments.list',compact('route_name','title','payments'));
    }
}
