<?php

namespace App\Http\Controllers\Admin;

use App\Bike;
use App\BikeModel;
use App\BikeType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

class BikeController extends Controller
{
    public function getBikes()
    {
        $search = $this->searchBikes();
        if ($search !== 0) {
            return $search;
        }
        $title = "Велосипеды";
        $route_name = Route::currentRouteName();
        $bikes = Bike::paginate(15);
        $bikes = $this->makeClasses($bikes);
        return view('admin.bikes.list',compact("bikes","route_name", "title"));
    }
    public function editBike($bike_id){
        $route_name = Route::currentRouteName();
        $bike = Bike::find($bike_id);
        $title = "Редактировать велосипед";
        $types = BikeType::all();
        $models = BikeModel::all();
        $status = DB::table('status')->get();
        $trips = $bike->trips()->orderBy('id', 'desc')->paginate(15);
        return view('admin.bikes.form', compact("route_name","bike","title","types","models","status","trips"));
    }

    public function makeBikeChanges(Request $request, $bike_id)
    {
        $request->validate([
            'parking_id'=>'required|exists:parkings,id',
            'type_id'=>'required|exists:bike_types,id',
            'model_id'=>'required|exists:bike_models,id',
            'status_id'=>'required|exists:status,id'
        ]);
        $bike = Bike::find($bike_id);
        if ($request->input('imei') != $bike->imei) {
            $request->validate([
                'imei'=>'required|numeric|unique:bikes'
            ]);
        }
        if ($request->input('serial_num') != $bike->serial_num) {
            $request->validate([
                'serial_num'=>'required|numeric|unique:bikes'
            ]);
        }
        if ($request->input('sim_num') != $bike->sim_num) {
            $request->validate([
                'sim_num'=>'required|numeric|unique:bikes'
            ]);
        }
        $bike->update($request->only('serial_num', 'sim_num', 'parking_id', 'type_id', 'model_id', 'latitude', 'longitude', 'status_id','imei'));
        return redirect()->back();
    }

    public function getAddBike()
    {
        $route_name = Route::currentRouteName();
        $title = "Добавить велосипед";
        $types = BikeType::all();
        $models = BikeModel::all();
        $status = DB::table('status')->get();
        return view('admin.bikes.form', compact("route_name","title","types","models","status"));
    }

    public function addBike(Request $request)
    {
        $request->validate([
            'parking_id'=>'required|exists:parkings,id',
            'serial_num'=>'required|numeric|unique:bikes',
            'sim_num'=>'required|numeric|unique:bikes',
            'type_id'=>'required|exists:bike_types,id',
            'model_id'=>'required|exists:bike_models,id',
            'status_id'=>'required|exists:status,id',
            'imei'=>'required'
        ]);
        Bike::create($request->all());
        $url = 'https://smsc.ru/sys/send.php?login=weedoo_velo&psw=DAkufyowlovcas8&phones='.$request->input('sim_num').'&sender=Poputi&mes=SERVER,0,89.108.64.26,2215,0#';
        file_get_contents($url);
        return redirect('/admin/bikes');
    }

    public function deleteBike($bike_id)
    {
        $bike = Bike::find($bike_id);
        $bike->lock->command()->create([
            'command'=>'SERVER,0,127.0.0.1,2215,0#'
        ]);
        $bike->delete();
        return redirect('/admin/bikes');
    }


    private function searchBikes()
    {
        if (isset($_GET['search_velo']))
        {
            $search = trim(urldecode($_GET['search_velo']));
            if (empty($search))
            {
                $bikes = Bike::paginate(15);
            }
            else
            {
                $bikes = Bike::orWhere('serial_num', 'LIKE', "%{$search}%")
                    ->orWhere('latitude', 'LIKE', "%{$search}%")
                    ->orWhere('longitude', 'LIKE', "%{$search}%")
                    ->orWhere('sim_num', 'LIKE', "%{$search}%")
                    ->get();
            }
            $statuses = DB::table('status')->get();
            foreach ($statuses as $key)
            {
                $firstWords = mb_substr($key->name, 0, 2);
                if ((mb_stripos($search, $firstWords) === 0))
                {
                    $bikes = Bike::where('status_id', $key->id)->get();
                }
            }
            $bikes = $this->makeClasses($bikes);
            return view('admin.bikes.table', compact('bikes'));
        }
        return 0;
    }

    private function makeClasses($bikes)
    {
        foreach ($bikes as $bike) {
            switch ($bike->status_id) {
                case 1:
                    $bike->class = 'tr_green';
                    break;
                case 2:
                    $bike->class = 'tr_pas';
                    break;
                case 3:
                    $bike->class = '';
                    break;
                case 4:
                    $bike->class = 'tr_yel';
                    break;
                case 5:
                    $bike->class = 'tr_pink';
                    break;
                case 6:
                    $bike->class = 'tr_red';
                    break;
            }
            $status = DB::table('status')->where('id', $bike->status_id)->first();
            $bike->status = $status->name;
            $trips = $bike->trips();
            if ($trips) {
                $last_trip = $trips->latest()->first();
                if ($last_trip) {
                    $bike->last_client = $last_trip->client;
                    $bike->last_trip = $last_trip;
                }else{
                    $bike->last_client = null;
                }
            }else{
                $bike->last_client = null;
            }

        }
        return $bikes;
    }
}
