<?php

namespace App\Schedule;

use App\Trip;
use App\Client;

class CalcTrips
{
    public static function calcTrips()
    {
        //get all unfinished trips
        $trips = Trip::where('done', 0)->get();
        foreach ($trips as $trip) {
            $trip->bike->lock->command()->create([
                'command'=>'GPSON#'
            ]);
            $tarif_type = $trip->tarif_type;
            //minute tariff
            if ($tarif_type->name == "Поминутно") {
                $passed_time = ceil((strtotime(date('Y-m-d H:i:s')) - strtotime($trip->created_at))/60);
                $client = $trip->client;
                $tarif = $trip->tarif_type->tarifs()->where('beg_minute','<=', $passed_time)->where('end_minute','>=', $passed_time)->first();
                if (!$tarif) {
                    $bike = $trip->bike;
                    $bike->status_id = 6;
                    $bike->save();
                    $trip->done = 1;
                    $trip->save();
                    $client->balance -= $trip->price;
                    $client->save();
                    continue;
                }
                $trip->price += $tarif->price;
                $trip->save();
                $next_time = $passed_time + 1;
                $next_tarif = $tarif_type->tarifs()->where('beg_minute','<=', $next_time)->where('end_minute','>=', $next_time)->first();
                if (!$tarif) {
                    $bike = $trip->bike;
                    $bike->status_id = 6;
                    $bike->save();
                    $trip->done = 1;
                    $trip->save();
                    $client->balance -= $trip->price;
                    $client->save();
                    continue;
                }
                if ($client->balance - ($trip->price + $next_tarif->price) <= 0) {
                    /*$trip->bike->lock->command()->create([
                        'command'=>'SDFIND,ON#'
                    ]);*/
                }
            }
        }

    }
}