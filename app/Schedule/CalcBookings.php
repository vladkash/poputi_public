<?php

namespace App\Schedule;

use App\Booking;
use App\Jobs\StopBooking;
use App\Tarif;
use App\TarifType;

class CalcBookings
{
    public static function calcBookings()
    {
        //take all unfinished bookings
        $bookings = Booking::where('done', 0)->get();
        foreach ($bookings as $booking) {
            //time left (in minutes)
            $passed_time = floor((strtotime(date('Y-m-d H:i:s')) - strtotime($booking->created_at))/60);
            $tarif_type = TarifType::where('name','Ожидание')->first();
            $tarif = $tarif_type->tarifs()->first();
            $price = $tarif->price;
            //free bookings have free first 10 minutes
            if ($booking->is_freee) {
                $general_price = $price * ($passed_time - 10);
            }else{
                $general_price = $price * $passed_time;
            }
            //if price for current moment of time more or equal 0
            if ($general_price >= 0) {
                //save price
                $booking->price = $general_price;
                $booking->save();
                $client = $booking->client;
                //if client hasn't enough money for next calculation, stop booking
                if ($client->balance - ($general_price + $price) < 0) {
                    StopBooking::dispatch($booking)->delay(now()->addSeconds(60-date('s')));
                }
            }
        }
    }

    public static function calcBooking($booking_id)
    {
        //take booking to calc
        $booking = Booking::where('done', 0)->find($booking_id);
        //if booking exists
        if ($booking) {
            //time left (in minutes)
            $passed_time = floor((strtotime(date('Y-m-d H:i:s')) - strtotime($booking->created_at))/60);
            $tarif_type = TarifType::where('name','Ожидание')->first();
            $tarif = $tarif_type->tarifs()->first();
            $price = $tarif->price;
            //free bookings have free first 10 minutes
            if ($booking->is_freee) {
                $general_price = $price * ($passed_time - 10);
            }else{
                $general_price = $price * $passed_time;
            }
            //if price for current moment of time <= 0
            if ($general_price >= 0) {
                //save price
                $booking->price = $general_price;
                $booking->save();
            }
        }

    }


}