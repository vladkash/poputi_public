<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $guarded = [];

    public function bike()
    {
        return $this->belongsTo('App\Bike');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
